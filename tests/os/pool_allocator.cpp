#include <gtest/gtest.h>

#include "core/os/pool_allocator.h"

struct TestObj
{
    int x;
    TestObj(int a) : x(a) {}
};

TEST(Memory, PoolAllocatorAlloc)
{
    PoolAllocator<TestObj> pa(10);

    EXPECT_EQ(pa.get_mem_used(), 0);
    EXPECT_EQ(pa.get_max_blocks(), 10);
    EXPECT_EQ(pa.get_num_allocs(), 0);

    TestObj* ta1 = pa.alloc();

    ASSERT_NE(ta1, nullptr);
    ta1->x = 10;
    EXPECT_EQ(ta1->x, 10);

    TestObj* ta2 = pa.alloc_new(12);
    
    ASSERT_NE(ta2, nullptr);
    EXPECT_EQ(ta2->x, 12);

    EXPECT_EQ(pa.get_mem_used(), 2 * sizeof(TestObj));
    EXPECT_EQ(pa.get_num_allocs(), 2);
    EXPECT_EQ(pa.get_max_blocks(), 10);
}

TEST(Memory, PoolAllocatorFree)
{
    PoolAllocator<TestObj> pa(10);

    TestObj* ta1 = pa.alloc();
    TestObj* ta2 = pa.alloc();
    TestObj* ta3 = pa.alloc();

    ta1->x = 10;
    ta2->x = 11;
    ta3->x = 12;

    pa.free(ta2);

    EXPECT_EQ(ta1->x, 10);
    EXPECT_EQ(ta3->x, 12);
    EXPECT_EQ(pa.get_num_allocs(), 2);
}

TEST(Memory, PoolAllocatorIterator)
{
    PoolAllocator<TestObj> pa(10);

    TestObj* ta1 = pa.alloc();
    TestObj* ta2 = pa.alloc();
    TestObj* ta3 = pa.alloc();

    ta1->x = 10;
    ta2->x = 11;
    ta3->x = 12;

    int i = 0;
    for (auto& obj : pa)
    {
        EXPECT_EQ(obj->x, i + 10);
        i++;
    }

    i = 0;
    for (const auto& obj : pa)
    {
        EXPECT_EQ(obj->x, i + 10);
        i++;
    }

    pa.free(ta2);

    auto it = pa.begin();
    EXPECT_EQ((*it)->x, 10);
    it++;
    EXPECT_EQ((*it)->x, 12);
}