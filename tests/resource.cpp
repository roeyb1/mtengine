#include <gtest/gtest.h>

#include "core/rid_manager.h"

class ResTestObj
{
public:
    int x;
    enum ConstructionType
    {
        Default,
        Regular,
        Copy,
        Move
    };
    ConstructionType construction;

    ResTestObj() : x(0), construction(Default) 
    {
    }

    ResTestObj(int a) : 
        x(a),
        construction(Regular) 
    {
    }

    ResTestObj(const ResTestObj& o) :
        x(o.x),
        construction(Copy)
    {
    }
    
    ResTestObj(ResTestObj&& o) :
        x(std::exchange(o.x, 0)),
        construction(Move)
    {
    }
};

class A
{
public:
    virtual uint32_t get_x() const = 0;
};

class B : public A
{
public:
    virtual uint32_t get_x() const override
    {
        return 10;
    }
};

TEST(References, RefCount)
{
    Ref<ResTestObj> a = make_ref<ResTestObj>(42);
    // Store memory allocated now to compare it to allocation later
#ifndef NDEBUG
    uint64_t allocdAmount = DefaultAllocator::getMemUsed();
#endif
    EXPECT_EQ((*a).x, 42);
    EXPECT_EQ(a->x, 42);
    
    EXPECT_EQ(a->construction, ResTestObj::ConstructionType::Regular);
    
    EXPECT_EQ(a.get_ref_count(), 1);
    Ref<ResTestObj> b = a;

    EXPECT_EQ(a, b);

    EXPECT_EQ(a.get_ref_count(), 2);

    // force destroy b to check if refcount updates
    b.unref();
    EXPECT_EQ(a.get_ref_count(), 1);

    // force destroy a to check if pointer is freed
    a.unref();
    EXPECT_EQ(a.get_ref_count(), 0);
#ifndef NDEBUG
    EXPECT_LT(DefaultAllocator::getMemUsed(), allocdAmount);
#endif
}

TEST(References, Inheritance)
{
   
    Ref<B> b = make_ref<B>();
    Ref<A> a = b;
    EXPECT_EQ(a.get_ref_count(), 2);
    EXPECT_EQ(b.get_ref_count(), 2);

    EXPECT_EQ(a->get_x(), 10);

    Ref<B> b2 = a;
    EXPECT_EQ(b2->get_x(), 10);
    EXPECT_EQ(b2.get_ref_count(), 3);
}

TEST(Resources, ResourceManager)
{
    RIDManager<ResTestObj> manager;
    
    Ref<ResTestObj> t1 = make_ref<ResTestObj>(42);
    Ref<ResTestObj> t2 = make_ref<ResTestObj>(43);

    RID id1 = manager.create_id(t1);
    RID id2 = manager.create_id(t2);
    EXPECT_EQ(id1, 0);
    EXPECT_EQ(id2, 1);

    EXPECT_EQ(t1, manager.get_resource(id1));
    EXPECT_EQ(t2, manager.get_resource(id2));
    EXPECT_THROW(manager.get_resource(10), std::out_of_range);
    
    EXPECT_EQ(manager.get_resource(id1)->x, 42);
    EXPECT_EQ(manager.get_resource(id2)->x, 43);

    manager.remove(id1);

    EXPECT_THROW(manager.get_resource(id1), std::out_of_range);
    EXPECT_EQ(t2, manager.get_resource(id2));
}