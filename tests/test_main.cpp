#include <gtest/gtest.h>
#include "core/os/memory.h"

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    int retVal = RUN_ALL_TESTS();
    return 0;
}