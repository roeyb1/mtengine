#include <gtest/gtest.h>

#include "ECS/ECS.h"

struct ComponentA
{
    COMPONENT_DEFINITION(ComponentA);
    int x;
};

struct ComponentB
{
    COMPONENT_DEFINITION(ComponentB);
    int x;
};

TEST(Stage, CreateEntity)
{
    Stage s;

    auto [eid, component] = s.create<ComponentA>();

    EXPECT_NE(component, nullptr);
}

TEST(EntityProxy, GetProxy)
{
    Stage s;

    auto[eid, component] = s.create<ComponentA>();
    auto[eid1, a2, b1] = s.create<ComponentA, ComponentB>();

    auto proxies = s.get_proxies<ComponentA>();
    ASSERT_EQ(proxies.size(), 2);

    ComponentA* c = proxies[0].get_mutable<ComponentA>();
    EXPECT_NE(c, nullptr);
    c->x = 10;

    const ComponentA* c_const = proxies[0].get_const<ComponentA>();
    EXPECT_EQ(c_const->x, 10);
}