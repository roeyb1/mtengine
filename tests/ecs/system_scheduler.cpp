#include <gtest/gtest.h>

#include "ECS/ECS.h"

struct TA
{
    COMPONENT_DEFINITION(TA);
    int x;
};

struct TB
{
    COMPONENT_DEFINITION(TB);
    int y;
};

struct TC
{
    COMPONENT_DEFINITION(TC);
    int z;
};

struct TD
{
    COMPONENT_DEFINITION(TD);
    int z;
};

FENCE_DEFINITION(F);

struct TS
{
    SINGLETON_COMPONENT(TS);
    int x;
};

class TestSysA : public System<TA>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            TA* a = proxy.get<TA>();

            a->x += 1;
        }
    }
};

class TestSysB : public System<TB>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            TB* b = proxy.get<TB>();

            b->y += 1;
        }
    }
};

class TestSysAB : public System<TA, TB>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            TA* a = proxy.get<TA>();
            TB* b = proxy.get<TB>();

            a->x = 10;
            b->y = 10;
        }
    }
};

TEST(SystemScheduler, RunScheduled)
{
    Stage s;

    s.register_system<TestSysA>();
    s.register_system<TestSysB>();
    s.register_system<TestSysAB>();

    auto [e1, a1] = s.create<TA>();
    auto [e2, b1] = s.create<TB>();
    auto [e3, a2, b2] = s.create<TA, TB>();

    s.initialize();

    s.update();
    s.update();
    s.update();

    EXPECT_EQ(a1->x, 3);
    EXPECT_EQ(a2->x, 11);
    EXPECT_EQ(b1->y, 3);
    EXPECT_EQ(b2->y, 11);
}

class TestSysC : public System<TC, F>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            TC* c = proxy.get<TC>();

            c->z = 10;            
        }
    }
};

class TestSysD : public System<TD, F>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            TD* d = proxy.get<TD>();

            d->z = 10;
        }
    }
};

class TestSysCD : public System<TC, TD, const F>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            TC* c = proxy.get<TC>();
            TD* d = proxy.get<TD>();
            c->z++;
            d->z++;
        }
    }
};

TEST(SystemScheduler, RunFenced)
{
    Stage s;

    s.register_system<TestSysC>();
    s.register_system<TestSysD>();
    s.register_system<TestSysCD>();

    auto [e1, c1] = s.create<TC>();
    auto [e2, d1] = s.create<TD>();
    auto [e3, c2, d2] = s.create<TC, TD>();

    s.initialize();

    s.update();
    s.update();
    s.update();

    EXPECT_EQ(c1->z, 10);
    EXPECT_EQ(c2->z, 11);
    EXPECT_EQ(d1->z, 10);
    EXPECT_EQ(d2->z, 11);
}

class TestSystemS1 : public System<TS>
{
public:
    void update() override final
    {
        TS* singleton = get_singleton<TS>();
        singleton->x = 10;
    }
};

class TestSystemS2 : public System<const TS>
{
public:
    void update() override final
    {
        const TS* singleton = get_singleton_const<TS>();
        EXPECT_EQ(singleton->x, 10);
    }
};

TEST(SystemScheduler, SingletonComponent)
{
    Stage s;

    s.register_system<TestSystemS1>();
    s.register_system<TestSystemS2>();

    s.initialize();
    s.update();
}

class TestSysABConst : public System<const TA, const TB>
{};

class TestSysAConst : public System<const TA>
{};

class TestSysBConst : public System<const TB>
{};

class TestSysBC : public System<TB, TC>
{};

class TestSysBCConst : public System<const TB, const TC>
{};

TEST(SystemScheduler, ManySystems)
{
    Stage s;

    s.register_system<TestSysA>();
    s.register_system<TestSysB>();
    s.register_system<TestSysAB>();
    s.register_system<TestSysC>();
    s.register_system<TestSysD>();
    s.register_system<TestSysCD>();
    s.register_system<TestSystemS1>();
    s.register_system<TestSystemS2>();
    s.register_system<TestSysABConst>();
    s.register_system<TestSysAConst>();
    s.register_system<TestSysBConst>();
    s.register_system<TestSysBCConst>();
    
    s.initialize();

    s.update();
}