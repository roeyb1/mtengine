#include <gtest/gtest.h>

#include "ECS/ECS.h"

struct A
{
    COMPONENT_DEFINITION(A);
    int x;
};

bool hasRunInitialize = false;

class SysA : public System<A>
{
public:
    void initialize() override final
    {
        auto [eid, a] = create_entity();
        a->x = 10;
    }

    void update() override final
    {
        EXPECT_EQ(get_new_proxies<A>().size(), 1);

        auto [eid, a] = create_entity();
        a->x = 10;

        for (auto& proxy : get_proxies())
        {
            auto a = proxy.get<A>();
            a->x++;
        }
    }

    void terminate() override final
    {
        hasRunInitialize = false;
    }
};

class SysAConst : public System<const A>
{
public:
    void initialize() override final
    {
        hasRunInitialize = true;
    }

    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            const A* a = proxy.get<A>();
            EXPECT_EQ(a->x, 11);

            EXPECT_NE(get_new_proxies<A>().size(), 0);
        }
    }
};

TEST(ECS, Initialize)
{
    Stage s;

    s.register_system<SysA>();
    s.register_system<SysAConst>();

    s.initialize();
    EXPECT_TRUE(hasRunInitialize);
    s.update();

    s.terminate();

    EXPECT_FALSE(hasRunInitialize);
}