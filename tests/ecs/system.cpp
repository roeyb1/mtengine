#include <gtest/gtest.h>

#include "ECS/ECS.h"

struct ComponentA
{
    COMPONENT_DEFINITION(ComponentA);
    int x;
};

struct ComponentB
{
    COMPONENT_DEFINITION(ComponentB);
    int y;
};

class TestSys : public System<ComponentA>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {            
            ComponentA* comp = proxy.get<ComponentA>();
            comp->x = 11;
        }
    }
};

class TestSysConst : public System<const ComponentA>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            // this will fail static assert since this system is not allowed to get non-cost ComponentA
            // ComponentA* comp_mut = proxy.get_mutable<ComponentA>();
            const ComponentA* comp = proxy.get<ComponentA>();
            comp->x == 10;
        }
    }
};

TEST(System, Constness)
{
    EXPECT_FALSE(TestSys::is_const_for<ComponentA>());
    EXPECT_TRUE(TestSysConst::is_const_for<ComponentA>());
}

class TestSystemA : public System<ComponentA, ComponentB>
{
public:
    void update() override final
    {
        for (auto& proxy : get_proxies())
        {
            ComponentA* a_comp = proxy.get<ComponentA>();
            ComponentB* b_comp = proxy.get<ComponentB>();

            a_comp->x = 20;
            b_comp->y = 21;
        }
    }
};

TEST(System, SingleComponent)
{
    Stage s;

    auto[eid1, a1, b1] = s.create<ComponentA, ComponentB>();
    auto[eid2, a2] = s.create<ComponentA>();
    a1->x = 1;
    a2->x = 10;

    s.register_system<TestSys>();

    s.initialize();
    s.update();

    EXPECT_EQ(a1->x, 11);
    EXPECT_EQ(a2->x, 11);
}

TEST(System, MultipleComponents)
{
    Stage s;

    auto[eid1, a1, b1] = s.create<ComponentA, ComponentB>();
    auto[eid2, a2] = s.create<ComponentA>();
    a1->x = 1;
    a2->x = 1;

    s.register_system<TestSystemA>();

    s.initialize();
    s.update();

    EXPECT_EQ(a1->x, 20);
    EXPECT_EQ(a2->x, 1);
    EXPECT_EQ(b1->y, 21);
};