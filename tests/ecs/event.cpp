#include <gtest/gtest.h>

#include "ECS/ECS.h"

struct TestEvent
{
    EVENT_DEFINITION(TestEvent);
    int x;
};

class EventCreater : public System<TestEvent>
{
public:
    void update() override final
    {
        TestEvent* t = create_event<TestEvent>();
        t->x = 10;
    }
};

class EventConsumer : public System<const TestEvent>
{
public:
    void update() override final
    {
        auto& testEvents = get_events<TestEvent>();
        EXPECT_EQ(testEvents.size(), 1);
        EXPECT_EQ(testEvents[0].x, 10);
    }
};

TEST(Events, Events)
{
    Stage s;
    s.register_system<EventCreater>();
    s.register_system<EventConsumer>();

    s.initialize();
    
    s.update();
    s.update();
    s.update();
}