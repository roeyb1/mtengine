# MTEngine

MTEngine is a cross-platform 2D and 3D game engine using an ECS architecture with a very simple user interface. The engine provides transparent multithreading by resolving system dependencies and building a multithreading graph based on component dependencies.

### Documentation

Documentation for the engine can be found on this repository's wiki [here](https://gitlab.com/roeyb1/mtengine/-/wikis/MTEngine-Docs).


# Examples

### Flappy Bird Clone

A flappy bird clone was created with this engine in less than 200 lines of code. The source for this project can be found [here](https://gitlab.com/roeyb1/flappy-clone).