#include "ECS/ECS.h"
#include "Systems/CoreSystems.h"

#include <tracy/Tracy.hpp>

#include "Components/Sprite.h"
#include "Components/Transform.h"

#include "Systems/ImGui/ImGuiControl.h"

#include "Systems/2D/CameraController2D.h"

#include "Components/Time.h"

#include "Systems/Rendering/RenderingControl.h"

class TestUI : public System<ImGuiControl>
{
public:
    void update() override final
    {
        ZoneScopedN("TestUI")
        ImGuiWindowFlags Flags = 0;
        Flags |= ImGuiWindowFlags_NoMove;
        Flags |= ImGuiWindowFlags_NoResize;
        Flags |= ImGuiWindowFlags_NoCollapse;
        Flags |= ImGuiWindowFlags_NoDecoration;
        Flags |= ImGuiWindowFlags_AlwaysAutoResize;
        Flags |= ImGuiWindowFlags_NoSavedSettings;
        Flags |= ImGuiWindowFlags_NoNav;
        Flags |= ImGuiWindowFlags_NoFocusOnAppearing;
        {
            ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiCond_Once);
            ImGui::SetNextWindowBgAlpha(0.35f);

            ImGui::Begin("FPS", nullptr, Flags);            
            ImGui::Text("%.1f FPS (%.3f ms/frame)", ImGui::GetIO().Framerate, 1000.0f / ImGui::GetIO().Framerate);
            ImGui::End();
        }
 

        ImGui::ShowDemoWindow();

        TracyPlot("FPS", ImGui::GetIO().Framerate);
    }
};

class TimedExit : public System<const Time>
{
public:
    void update() override final
    {
        ZoneScopedN("TimedExit")
        static double counter = 0;
        const Time* time = get_singleton_const<Time>();
        counter+= time->deltaTime;

        //if (counter > 5000)
        //    signal_quit();
    }
};

int main(int argc, const char** argv)
{
    Stage s;
    
    RegisterCoreSystems(s);
    s.register_system<TimedExit>();
    s.register_system<TestUI>();

    s.initialize();

    s.get_scheduler().print_graphviz();

    for (int i = 0; i < 100; ++i)
    {
        auto [eid, sprite, transform] = s.create<Sprite, Transform>();
        transform->Pos = {- 512 + 128 * (i % 10), -512 + 128 * (i / 10), 0};
        transform->Rotation = {0, 0, 0};
        transform->Scale = glm::vec3(64, 64, 1);
        sprite->TexturePath = "assets/textures/test3.png";
    }

    while (s.update()) {}
    
    s.terminate();

    return 0;
}