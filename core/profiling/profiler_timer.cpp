#include "profiler_timer.h"
#include <algorithm>
#include <thread>

#include "profiler.h"

ProfilerTimer::ProfilerTimer(const char* name) : 
    name(name), 
    stopped(false)
{
    startPoint = std::chrono::high_resolution_clock::now();
}

ProfilerTimer::~ProfilerTimer()
{
    if (!stopped)
        stop();
}

void ProfilerTimer::stop()
{
    auto endPoint = std::chrono::high_resolution_clock::now();

    long long start = std::chrono::time_point_cast<std::chrono::microseconds>(startPoint).time_since_epoch().count();
    long long end = std::chrono::time_point_cast<std::chrono::microseconds>(endPoint).time_since_epoch().count();

    uint32_t threadID = std::hash<std::thread::id>{}(std::this_thread::get_id());
    Profiler::get().write_profile({ name, start, end, threadID });

    stopped = true;
}