#ifndef PROFILER_H
#define PROFILER_H

#include <fstream>
#include <string>
#include <mutex>

#include "profiler_timer.h"

#ifdef PROFILING_ENABLED
#define PROFILE(name) ProfilerTimer profilerTimer(name)
#define PROFILE_FUNCTION() PROFILE(__PRETTY_FUNCTION__)
#else
#define PROFILE(name)
#define PROFILE_FUNCTION()
#endif

struct ProfileResult
{
    std::string name;
    long long start, end;
    uint32_t threadID;
};


class Profiler
{
private:
    std::string sessionName;
    std::ofstream outputStream;
    int profileCount;

    bool running;

    std::mutex mutex;

    Profiler() :
        profileCount(0),
        running(false) 
    {}

public:
    void begin_session(const std::string& name, const std::string& filepath = "benchmark.json");
    void end_session();
    
    void write_profile(const ProfileResult& result);

    void write_header();
    void write_footer();

    static Profiler& get() { static Profiler p; return p; }

};

#endif // PROFILE_H