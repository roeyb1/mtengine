#ifndef PROFILER_TIMER_H
#define PROFILER_TIMER_H

#include <chrono>

class ProfilerTimer
{
    private:
    const char* name;
    bool stopped;
    
    std::chrono::time_point<std::chrono::high_resolution_clock> startPoint;
public:
    ProfilerTimer(const char* name);

    ~ProfilerTimer();
    

    void stop();
};

#endif // PROFILER_TIMER_H