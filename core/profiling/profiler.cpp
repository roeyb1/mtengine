#include "profiler.h"
#include <algorithm>

void Profiler::begin_session(const std::string& name, const std::string& filepath)
{
    std::lock_guard<std::mutex> lock(mutex);
    outputStream.open(filepath);
    write_header();
    sessionName = name;
    running = true;

}

void Profiler::end_session()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!running)
        return;
    write_footer();
    outputStream.close();
    profileCount = 0;
    running = false;

}

void Profiler::write_profile(const ProfileResult& result)
{
    std::lock_guard<std::mutex> lock(mutex);

    if (profileCount++ > 0)
        outputStream << ",";

    std::string name = result.name;
    std::replace(name.begin(), name.end(), '"', '\'');

    outputStream << "{";
    outputStream << "\"cat\":\"function\",";
    outputStream << "\"dur\":" << (result.end - result.start) << ',';
    outputStream << "\"name\":\"" << name << "\",";
    outputStream << "\"ph\":\"X\",";
    outputStream << "\"pid\":0,";
    outputStream << "\"tid\":" << result.threadID << ",";
    outputStream << "\"ts\":" << result.start;
    outputStream << "}";

    outputStream.flush();

}

void Profiler::write_header()
{
    outputStream << "{\"otherData\": {},\"traceEvents\":[";
    outputStream.flush();

}

void Profiler::write_footer()
{
    outputStream << "]}";
    outputStream.flush();

}