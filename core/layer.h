#ifndef LAYER_H
#define LAYER_H

#include "reference.h"
#include "events/event.h"
#include <string>

class Layer
{
public:
    Layer(const std::string& name = "Layer") : layer_name(name) {}
    virtual ~Layer() {}

    virtual void on_bind() {}
    virtual void on_unbind() {}

    virtual void begin_frame() {}

    virtual void on_update(float dt) {}
    virtual void on_fixed_update() {}

    virtual void end_frame() {}
    
    virtual void on_event(Ref<Event> e) = 0;
protected:
    std::string layer_name;
};

#endif // LAYER_H