#ifndef INPUT_CODES_H
#define INPUT_CODES_H

// for now just use glfw key codes
using KeyCode = uint32_t;

enum MouseCode
{
    BOTTON_NONE,
    BUTTON_LEFT,
    BUTTON_RIGHT,
    BUTTON_MIDDLE
};

enum InputModifier
{
    MOD_NONE = 0,
    MOD_SHIFT = BIT(1),
    MOD_CTRL = BIT(2),
    MOD_ALT = BIT(3),
    MOD_SUPER = BIT(4)
};

#endif // INPUT_CODES_H