#ifndef LOGGER_H
#define LOGGER_H

#include <stdarg.h>

class Logger
{
public:
    enum Severity
    {
        INFO,
        WARNING,
        ERROR
    };

    void logf(const char* message, ...);

    void logf(Severity severity, const char* message, ...);
};

#endif // LOGGER_H