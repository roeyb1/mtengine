#include "logger.h"
#include <stdio.h>
#include <string>

#include <tracy/Tracy.hpp>

void Logger::logf(const char* message, ...)
{
    ZoneScoped
    va_list args;
    va_start(args, message);

    vprintf(message, args);
    
    va_end(args);
}

void Logger::logf(Severity severity, const char* message, ...)
{
    ZoneScoped
    va_list args;
    va_start(args, message);
    const char* prefix = nullptr;

    switch (severity)
    {
        case INFO:      prefix = "INFO: "; break;
        case WARNING:   prefix = "WARN: "; break;
        case ERROR:     prefix = "ERR: ";  break;
        default:        break;
    }

    std::string result = std::string(prefix) + std::string(message);
    
    vprintf(result.c_str(), args);
    
    va_end(args);
}