#ifndef RID_H
#define RID_H

#include <cstdint>

#define INVALID_RID UINT64_MAX

using RID = uint64_t;

#endif // RID_H