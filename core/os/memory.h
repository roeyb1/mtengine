#ifndef MEMORY_H
#define MEMORY_H

#include <cstring>
#include <cstdint>
#include <cassert>
#include <cstdio>
#include <atomic>

#define copymem(to, from, count)    memcpy(to, from, count)
#define movemem(to, from, count)    memmove(to, from, count)
#define zeromem(to, count)          memset(to, 0, count)

// Add 16 bytes to the start of allocs in debug mode to store metadata
// Used to keep track of used memory in debug mode to track leaks.
// Also used to store array lengths
#define PAD_AMOUNT 16

class DefaultAllocator
{
private:

#ifndef NDEBUG
    static std::atomic<uint64_t> mem_max;
    static std::atomic<uint64_t> mem_used;
#endif

    static std::atomic<uint64_t> num_allocs;
public:
    
    static void* alloc_(size_t size, bool prepad = false);
    static void* realloc_(void* ptr, size_t size, bool prepad = false);
    static void free_(void* ptr, bool prepad = false);

#ifndef NDEBUG
    static uint64_t getMemMax() { return mem_max;}
    static uint64_t getMemUsed() { return mem_used; }
#endif
    static uint64_t getNumAllocs() { return num_allocs; }
};

void* operator new(size_t size, const char* desc);

inline void* operator new(size_t size, void* ptr, const char* desc)
{
    return ptr;
}

#define memalloc(size) DefaultAllocator::alloc_(size)
#define memreallic(size) DefaultAllocator::realloc_(size)
#define memfree(ptr) DefaultAllocator::free_(ptr)

#define memnew(class) (new ("") class)

template <class T>
void _memdelete(T* obj)
{
    if (obj)
        obj->~T();
    
    DefaultAllocator::free_(obj);
}

#define memdelete(obj) _memdelete(obj);


template <typename T>
T* _memnew_arr(size_t count)
{
    if (count == 0) return 0;

    // Always pad when allocating an array in order to store length
    size_t alloc_size = count * sizeof(T) + PAD_AMOUNT;
    
    T* mem = static_cast<T*>(DefaultAllocator::alloc_(alloc_size));
    assert(mem != nullptr);

    // Store the length
    uint64_t* s = (uint64_t*)(mem);
    *s = count;

    // Call new on all elements in the array

    T* elements = (T*)((uintptr_t)mem + PAD_AMOUNT);
    for (size_t i = 0; i < count; ++i)
    {
        new (&elements[i], "") T;
    }

    return static_cast<T*>(elements);
}

#define memnew_arr(class, count) _memnew_arr<class>(count)


template <typename T>
uint64_t _arr_len(const T* ptr)
{
    uint64_t* s = (uint64_t*)((uintptr_t)ptr - PAD_AMOUNT);
    return *s;
}

#define arr_len(arr) _arr_len(arr)


template<typename T>
void memdelete_arr(T* obj)
{
    void* mem = (void*)((uintptr_t)obj - PAD_AMOUNT);
    uint64_t count = *(uint64_t*)mem;

    for (uint64_t i = 0; i < count; ++i)
    {
        obj[i].~T();
    }

    DefaultAllocator::free_(mem);
}

#endif // MEMORY_H