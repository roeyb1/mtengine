#ifndef THREAD_SAFE_QUEUE
#define THREAD_SAFE_QUEUE

#include "core/reference.h"
#include "thread_safe.h"

#include <queue>
#include <condition_variable>

template <class T>
class ThreadsafeQueue
{
    THREAD_SAFE_CLASS
    std::queue<T> data;
    std::condition_variable cond;

    std::atomic<bool> end = ATOMIC_FLAG_INIT; // initialize to false
public:
    ThreadsafeQueue() {}
    
    /**
     * Copy constructor
     */
    ThreadsafeQueue(const ThreadsafeQueue& other)
    {
        std::lock_guard<std::mutex> lock(other.mutex);
        data = other.data;
    }
    /**
     * Push new data into the queue
     */
    void push(T val)
    {
        THREAD_SAFE_METHOD
        data.push(val);
        cond.notify_one();
    }

    /**
     * Waits for data to be available to pop and then sets val to the data.
     */
    void wait_and_pop(T& val)
    {
        std::unique_lock<std::mutex> lock(mutex);
        cond.wait(lock, [this]{ return !data.empty() || end.load(); });

        // This will only be true when end is true since the thread won't wake
        // up until either there is something in the queue or end is set to true.
        if (data.empty()) return;

        val = data.front();
        data.pop();
    }

    /**
     * Waits for data to be available and then returns a shared pointer to the data.
     */
    Ref<T> wait_and_pop()
    {
        std::unique_lock<std::mutex> lock(mutex);
        cond.wait(lock, [this]{ return !data.empty() || end.load(); });

        // This will only be true when end is true since the thread won't wake
        // up until either there is something in the queue or end is set to true.
        if (data.empty()) return Ref<T>();

        Ref<T> result(make_ref<T>(data.front()));
        data.pop();
        return result;
    }

    /**
     * Attempts to pop the queue. If there is nothing in the queue, false is returned
     * and val is not modified. If there is something, return true and set val to the data.
     */
    bool try_pop(T& val)
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (data.empty())
            return false;
        cond.wait(lock, [this]{ return !data.empty() || end.load(); });

        // This will only be true when end is true since the thread won't wake
        // up until either there is something in the queue or end is set to true.
        if (data.empty()) return false;

        val = data.front();
        data.pop();
        return true;
    }

    /**
     * Attempts to pop the queue. If there is nothing in the queue, nullptr is returned.
     * If there is something, return a shared pointer to it.
     */
    Ref<T> try_pop()
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (data.empty())
            return Ref<T>();
        cond.wait(lock, [this]{ return !data.empty() || end.load(); });

        // This will only be true when end is true since the thread won't wake
        // up until either there is something in the queue or end is set to true.
        if (data.empty()) return Ref<T>();

        Ref<T> result(make_ref<T>(data.front()));
        data.pop();
        return result;
    }

    /**
     * Returns true if the queue is empty
     */
    const bool empty()
    {
        THREAD_SAFE_METHOD
        return data.empty();
    }

    void terminate()
    {
        THREAD_SAFE_METHOD
        end.store(true);
        cond.notify_all();
    }
};

#endif // THREAD_SAFE_QUEUE