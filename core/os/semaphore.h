#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <mutex>
#include <condition_variable>

class Semaphore
{
private:
    mutable std::mutex mutex;
    mutable std::condition_variable cond;
    mutable unsigned long count = 0;
public:

    const void signal()
    {
        std::lock_guard<std::mutex> lock(mutex);
        ++count;
        cond.notify_one();
    }

    const void wait()
    {
        std::unique_lock<std::mutex> lock(mutex);
        while (!count) // wait until there wakeup
            cond.wait(lock);
        --count;
    }

    const bool try_wait()
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (count)
        {
            --count;
            return true;
        }
        return false;
    }
};

#endif // SEMAPHORE_H