#ifndef POOL_ALLOCATOR_H
#define POOL_ALLOCATOR_H

#include <cstdint>
#include <set>
#include <algorithm>
#include <utility>

#include "memory.h"

template <typename T>
using PoolIterator = std::set<T*>::iterator;

template <typename T>
class PoolAllocator
{
public:
    PoolAllocator(size_t numBlocks) : 
        numBlocks(numBlocks),
        numAllocs(0)
    {
        startAddr = (T*)memalloc(sizeof(T) * numBlocks);
        clear();
    }

    ~PoolAllocator()
    {
        memfree(startAddr);
    }

    T* alloc()
    {
        if (numAllocs == numBlocks) return nullptr;

        T* res = freeList.extract(freeList.begin()).value();
        numAllocs++;
        used.insert(res);
        return res;
    }

    template <typename ...Args>
    T* alloc_new(Args&& ... args)
    {
        T* res = alloc();
        *res = T(std::forward<Args>(args)...);
        return res;
    }

    void free(T* ptr)
    {
        freeList.insert(ptr);
        used.erase(used.find(ptr));
        numAllocs--;
    }

    void clear()
    {
        for (int i = 0; i < numBlocks; ++i)
        {
            freeList.insert(startAddr + i);
        }
        numAllocs = 0;
    }

    PoolIterator<T> begin()
    {
        return used.begin();
    }

    const PoolIterator<T> begin() const
    {
        return used.begin();
    }

    PoolIterator<T> end()
    {
        return used.end();
    }

    const PoolIterator<T> end() const
    {
        return used.end();
    }

    size_t get_max_blocks() { return numBlocks; }
    size_t get_num_allocs() { return numAllocs; }
    size_t get_mem_used() { return numAllocs * sizeof(T); }
private:
    T* startAddr = nullptr;
    size_t numBlocks;
    size_t numAllocs;
    // Maintain a sorted list of free pointers (in RB tree)
    std::set<T*> freeList;
    // Maintain a sorted list of used pointers (for easy iterators)
    std::set<T*> used;
};

#endif // POOL_ALLOCATOR_H