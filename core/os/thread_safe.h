#ifndef THREAD_SAFE_H
#define THREAD_SAFE_H

#include <mutex>

#define THREAD_SAFE_CLASS   mutable std::mutex mutex;
#define THREAD_SAFE_METHOD  std::lock_guard<std::mutex> lock(mutex);
#define THREAD_SAFE_LOCK    mutex.lock();
#define THREAD_SAFE_UNLOCK  mutex.unlock();

#endif // THREAD_SAFE_H