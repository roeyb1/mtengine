#include "memory.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <tracy/Tracy.hpp>

#ifndef NDEBUG
std::atomic<uint64_t> DefaultAllocator::mem_max = ATOMIC_FLAG_INIT;
std::atomic<uint64_t> DefaultAllocator::mem_used = ATOMIC_FLAG_INIT;
#endif

std::atomic<uint64_t> DefaultAllocator::num_allocs = ATOMIC_FLAG_INIT;

void* DefaultAllocator::alloc_(size_t size, bool prepad)
{
#ifndef NDEBUG
    prepad = true;
#endif

    void* mem = malloc(size + (prepad ? PAD_AMOUNT : 0));
    TracyAlloc(mem, size + (prepad ? PAD_AMOUNT : 0));
    
    assert(mem != nullptr);

    num_allocs++;

    if (prepad)
    {
        uint64_t* s = static_cast<uint64_t*>(mem);
        *s = size;
#ifndef NDEBUG
        mem_used += size;

        if (mem_used > mem_max)
            mem_max.store(mem_used);
#endif

    return (void*)((uintptr_t)mem + PAD_AMOUNT);

    }
    else
        return mem;
}

void DefaultAllocator::free_(void* ptr, bool prepad)
{
#ifndef NDEBUG
    prepad = true;
#endif

    void* mem = ptr;

    if (prepad)
    {
        mem = (void*)((uintptr_t)ptr - PAD_AMOUNT);
#ifndef NDEBUG
        uint64_t* s = static_cast<uint64_t*>(mem);
        
        mem_used -= *s;
#endif
    }

    num_allocs--;

    TracyFree(mem);
    free(mem);
}

void* operator new(size_t size, const char* desc)
{
    return DefaultAllocator::alloc_(size);
}