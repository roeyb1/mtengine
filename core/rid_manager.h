#ifndef RID_MANAGER_H
#define RID_MANAGER_H

#include <map>
#include <atomic>

#include "rid.h"
#include "reference.h"
#include "core/os/thread_safe.h"

class RIDGenerator
{
public:
    static RID make_new_id() { return nextID++; }
private:
    static std::atomic<uint64_t> nextID;
};

template <typename T>
class RIDManager
{
    THREAD_SAFE_CLASS
public:
    RIDManager()
    {

    }

    ~RIDManager()
    {

    }

    RID create_id(Ref<T>& val)
    {
        //THREAD_SAFE_METHOD
        RID id = RIDGenerator::make_new_id();
        resourceMap.insert(std::pair<RID, Ref<T>>(id, Ref<T>(val)));
        return id;
    }

    Ref<T>& get_resource(RID id)
    {
        //THREAD_SAFE_METHOD
        return resourceMap.at(id);
    }

    Ref<T>& get_resource(RID id) const
    {
        //THREAD_SAFE_METHOD
        return resourceMap.at(id);
    }

    void remove(RID id)
    {
        //THREAD_SAFE_METHOD
        resourceMap.erase(id);
    }

    void clear()
    {
        //THREAD_SAFE_METHOD
        resourceMap.clear();
    }

    size_t size() const
    {
        //THREAD_SAFE_METHOD
        return resourceMap.size();
    }

    auto begin()
    {
        //THREAD_SAFE_METHOD
        return resourceMap.begin();
    }

    auto begin() const
    {
        //THREAD_SAFE_METHOD
        return resourceMap.begin();
    }

    auto end()
    {
        THREAD_SAFE_METHOD
        return resourceMap.end();
    }

    auto end() const
    {
        //THREAD_SAFE_METHOD
        return resourceMap.end();
    }
private:
    std::map<RID, Ref<T>> resourceMap;
};

#endif // RID_MANAGER_H