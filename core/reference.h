#ifndef REFERENCE_H
#define REFERENCE_H

#include <memory>
#include <utility>
#include "core/os/memory.h"


template <class T>
class Ref
{
public:
    Ref()
    {
        ptr = nullptr;
    }

    Ref(const Ref& other)
    {
        copy(other);
    }

    template <typename Y>
    Ref(const Ref<Y>& from)
    {
        ptr = dynamic_cast<T*>(from.ptr);
        refCount = from.refCount;
        refCount->fetch_add(1);
    }

    Ref(T* _ptr)
    {
        refCount = memnew(std::atomic<uint64_t>(ATOMIC_FLAG_INIT));
        refCount->store(1);
        ptr = _ptr;
    }

    ~Ref()
    {
        unref();
    }

    T* operator->()
    {
        return ptr;
    }

    T& operator*()
    {
        return *ptr;
    }

    const T* operator->() const
    {
        return ptr;
    }

    const T& operator*() const
    {
        return ptr;
    }

    Ref& operator=(Ref other)
    {
        copy(other);
        return *this;
    }
    
    bool operator==(const Ref& other) const
    {
        return ptr == other.ptr;
    }

    bool operator!=(const Ref& other) const
    {
        return ptr != other.ptr;
    }

    uint64_t get_ref_count() { return refCount == nullptr ? 0 : refCount->load(); }

    void unref()
    {
        if (ptr == nullptr && refCount == nullptr) return;
        if (refCount->load() == 1)
        {
            memdelete(ptr);
            refCount->store(0);
            memdelete(refCount);
        }
        else
            refCount->fetch_sub(1);

        refCount = nullptr;
        ptr = nullptr;
    }

private:
    T* ptr = nullptr;
    std::atomic<uint64_t>* refCount = nullptr;

    // friend class all instances of this template with each other
    template <class ref>
    friend class Ref;

    void copy(const Ref& other)
    {
        //unref();
        other.refCount->fetch_add(1);
        ptr = other.ptr;
        refCount = other.refCount;
    }
};

template <typename T, typename ...Args>
inline Ref<T> make_ref(Args&& ... args)
{
    T* ptr = memnew(T(std::forward<Args>(args)...));
    return Ref<T>(ptr);
}

#endif // REFERENCE_H