#ifndef RESIZE_EVENT_H
#define RESIZE_EVENT_H

#include "ECS/Event.h"

#include <cstdint>

struct ResizeEvent
{
    EVENT_DEFINITION(ResizeEvent);
    uint32_t newWidth, newHeight;
};

#endif // RESIZE_EVENT_H