#include "CoreSystems.h"

#include "ECS/Stage.h"

/** Time keeping **/
#include "TimeControl.h"

/* Rendering systems */
#include "Rendering/WindowControl.h"
#include "Rendering/RenderingSetup.h"
#include "Rendering/RenderingPresent.h"

/** UI **/
#include "ImGui/ImGuiSubmit.h"

/** 2D **/
#include "2D/CameraController2D.h"
#include "2D/SpriteUpdate.h"
#include "2D/DrawSprites.h"

/** Debug **/
#ifndef NDEBUG
#include "DebugUI/DebugUI.h"
#endif

void RegisterCoreSystems(Stage& stage)
{
    ZoneScoped
    /** Time control **/
    stage.register_system<TimeControl>();
    
    /** Rendering **/
    stage.register_system<WindowControl>();
    stage.register_system<RenderingSetup>();
    stage.register_system<RenderingPresent>();

    /** ImGui **/
    stage.register_system<ImGuiSubmit>();

    /** 2D **/
    stage.register_system<CameraController2D>();
    stage.register_system<SpriteUpdate>();
    stage.register_system<DrawSprites>();

#ifndef NDEBUG
    stage.register_system<DebugUI>();
#endif
}