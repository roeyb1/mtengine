#ifndef DEBUG_UI_H
#define DEBUG_UI_H

#include "ECS/System.h"
#include "Systems/ImGui/ImGuiControl.h"
#include "Components/Time.h"

class DebugUI : public System<ImGuiControl, const Time>
{
public:
    void update() override final
    {
        const Time* TimePtr = get_singleton_const<Time>();
        ImGuiWindowFlags Flags = 0;
        Flags |= ImGuiWindowFlags_NoResize;
        Flags |= ImGuiWindowFlags_AlwaysAutoResize;
        Flags |= ImGuiWindowFlags_NoFocusOnAppearing;
        Flags |= ImGuiWindowFlags_NoNav;
        Flags |= ImGuiWindowFlags_NoDecoration;

        ImGui::Begin("ECS Debug Stats", nullptr, Flags);
        ImGui::Text("Statistics");
        ImGui::Separator();
        ImGui::Text("Uptime: %.2f s", TimePtr->Uptime);
        ImGui::Text("Number of Workers: %d", stage->GetNumberOfWorkers());
        ImGui::Text("Systems: %d", stage->GetNumberOfSystems());
        ImGui::End();
    }
};

#endif // DEBUG_UI_H