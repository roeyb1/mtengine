#ifndef WIDNOW_CONTROL_H
#define WINDOW_CONTROL_H

#include "ECS/System.h"
#include "Components/Window.h"
#include "WindowReady.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

class VulkanContext;

class WindowControl : public System<Window, WindowReady>
{
public:
    void initialize() override final;

    void update() override final;

    void terminate() override final;
};
#endif // WINDOW_CONTROL_H