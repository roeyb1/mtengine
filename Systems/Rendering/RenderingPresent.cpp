#include "RenderingPresent.h"

void RenderingPresent::update()
{
    ZoneScopedN("RenderingPresent")
    auto context = get_singleton<VulkanContext>();
    
    /*
    {
        ZoneScopedN("UpdateModels");
        // update push constants for dynamic objects
        for (auto& proxy : get_partial_proxy<Drawable>())
        {
            const Drawable* drawable = proxy.get_const<Drawable>();
            if (drawable->rendererID != INVALID_RID)
            {
                context->UpdateModel(drawable->rendererID, drawable->model);
            }
        }
    }

    const OrthoCamera* Camera = get_singleton_const<OrthoCamera>();
    context->UpdateCameraUBO(Camera->View, Camera->Projection);
    */

    context->UpdateFrameData();
    context->PresentFrame();
}