#ifndef RENDERING_CONTROL_H
#define RENDERING_CONTROL_H

#include "ECS/Component.h"

FENCE_DEFINITION(RenderingBegin);
FENCE_DEFINITION(DrawFence);

#endif // RENDERING_CONTROL_H