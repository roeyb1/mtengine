#ifndef RENDERING_PRESENT_H
#define RENDERING_PRESENT_H

#include "ECS/System.h"

#include "Components/Window.h"

#include "Events/ResizeEvent.h"
#include "RenderingControl.h"

#include "Drivers/Vulkan/VulkanContext.h"

#include "Systems/ImGui/ImGuiControl.h"

#include "Components/Camera.h"

class RenderingPresent : public System<VulkanContext, const OrthoCamera, const DrawFence, const RenderingBegin>
{
public:
    void update() override final;
};

#endif // RENDERING_PRESENT_H