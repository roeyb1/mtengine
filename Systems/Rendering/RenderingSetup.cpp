#include "RenderingSetup.h"

#include <tracy/Tracy.hpp>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <imgui/imgui.h>
#include <imgui/examples/imgui_impl_glfw.h>

void RenderingSetup::initialize()
{
    ZoneScoped
    VulkanContext* VkCtx = get_singleton<VulkanContext>();
    VkCtx->SetWindowPtr(get_singleton<Window>());
    VkCtx->InitVulkan();
}

void RenderingSetup::update()
{
    ZoneScopedN("RenderingSetup")
    VulkanContext* VkCtx = get_singleton<VulkanContext>();
    
    // Aquires the next image from the swapchain
    VkCtx->PrepareNextFrame();
}

void RenderingSetup::terminate()
{
    ZoneScoped
    auto context = get_singleton<VulkanContext>();
    context->CleanupRenderingResources();
}