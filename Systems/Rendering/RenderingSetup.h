#ifndef RENDERING_SETUP_H
#define RENDERING_SETUP_H

#include "ECS/ECS.h"

#include "Components/Window.h"

#include "RenderingControl.h"
#include "Events/ResizeEvent.h"

#include "Drivers/Vulkan/VulkanContext.h"

#include "WindowReady.h"

class RenderingSetup : public System<VulkanContext, RenderingBegin, Window, const WindowReady>
{
public:
    void initialize() override final;

    void update() override final;

    void terminate() override final;
};

#endif // RENDERING_SETUP_H