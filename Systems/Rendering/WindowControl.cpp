#include "WindowControl.h"

#include "Events/ResizeEvent.h"

#include "Drivers/Vulkan/VulkanContext.h"

void WindowControl::initialize()
{
    Window* WindowPtr = get_singleton<Window>();
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    WindowPtr->window = glfwCreateWindow(INITIAL_WIDTH, INITIAL_HEIGHT, "MTEngine", nullptr, nullptr);
}

void WindowControl::update()
{
    ZoneScopedN("WindowControl")
    Window* WindowPtr = get_singleton<Window>();
    if (glfwWindowShouldClose(WindowPtr->window))
    {
        signal_quit();
    }

    // Fire off a window resize event if the window was resized last frame
    if (WindowPtr->bHasResizeEvent)
    {
        ResizeEvent* Event = create_event<ResizeEvent>();
        Event->newWidth = WindowPtr->windowWidth;
        Event->newHeight = WindowPtr->windowHeight;
        
        // Reset the event flag
        WindowPtr->bHasResizeEvent = false;
    }
}

void WindowControl::terminate()
{
    Window* WindowPtr = get_singleton<Window>();
    glfwDestroyWindow(WindowPtr->window);
    glfwTerminate();
}