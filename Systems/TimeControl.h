#ifndef TIME_CONTROL_H
#define TIME_CONTROL_H

#include "ECS/System.h"
#include "Components/Time.h"

class TimeControl : public System<Time>
{
public:
    void initialize() override final
    {
        Time* time = get_singleton<Time>();
        time->currentTime = std::chrono::high_resolution_clock::now();
    }
    void update() override final
    {
        Time* time = get_singleton<Time>();
        time->lastTime = time->currentTime;
        time->currentTime = std::chrono::high_resolution_clock::now();
        time->deltaTime = std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(time->currentTime - time->lastTime).count();
        time->Uptime += time->deltaTime / 1000.f;
    }
};

#endif // TIME_CONTROL_H