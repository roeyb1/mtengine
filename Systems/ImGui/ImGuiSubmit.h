#ifndef IMGUI_SUBMIT_H
#define IMGUI_SUBMIT_H

#include "ECS/System.h"

struct Window;

#include "Drivers/Vulkan/ImGuiVulkan.h"
#include "Systems/ImGui/ImGuiControl.h"
#include "Systems/Rendering/RenderingControl.h"

class ImGuiSubmit : public System<const Window, const VulkanContext, ImGuiCtx, const ImGuiControl, DrawFence>
{
public:
    void initialize() override final;

    void update() override final;

    void terminate() override final;
};

#endif // IMGUI_SUBMIT_H