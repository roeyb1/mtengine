#ifndef IMGUI_CONTROL_H
#define IMGUI_CONTROL_H

#include "ECS/Component.h"

#include <imgui/imgui.h>

struct ImGuiControl
{
    COMPONENT_DEFINITION(ImGuiControl);
};

#endif // IMGUI_CONTROL_H