#include "ImGuiSubmit.h"

#include <imgui/imgui.h>
#include <imgui/examples/imgui_impl_vulkan.h>
#include <imgui/examples/imgui_impl_glfw.h>

#include "Components/Window.h"

#include "Drivers/Vulkan/VulkanContext.h"
#include "Drivers/Vulkan/ImGuiVulkan.h"

static void CreateRenderingResources(ImGuiCtx* ImguiCtx, const VulkanContext* VkCtx)
{
    ZoneScoped
    ImGuiIO& io = ImGui::GetIO();
    // Font texture data
    unsigned char* FontData;
    int TexWidth;
    int TexHeight;

    io.Fonts->GetTexDataAsRGBA32(&FontData, &TexWidth, &TexHeight);

    VkDeviceSize UploadSize = TexWidth * TexHeight * 4 * sizeof(char);

    ImguiCtx->Font = VkCtx->CreateTexture(FontData, TexWidth, TexHeight, false);

    VkVertexInputBindingDescription VertexInputBinding{};
    VertexInputBinding.binding = 0;
    VertexInputBinding.stride = sizeof(ImDrawVert);
    VertexInputBinding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;


    std::vector<VkVertexInputAttributeDescription> VertexInputAttributes;
    VkVertexInputAttributeDescription PosAttribute{};
    PosAttribute.binding = 0;
    PosAttribute.location = 0;
    PosAttribute.format = VK_FORMAT_R32G32_SFLOAT;
    PosAttribute.offset = offsetof(ImDrawVert, pos);
    VertexInputAttributes.push_back(PosAttribute);

    VkVertexInputAttributeDescription UVAttribute{};
    UVAttribute.binding = 0;
    UVAttribute.location = 1;
    UVAttribute.format = VK_FORMAT_R32G32_SFLOAT;
    UVAttribute.offset = offsetof(ImDrawVert, uv);
    VertexInputAttributes.push_back(UVAttribute);

    VkVertexInputAttributeDescription ColorAttribute{};
    ColorAttribute.binding = 0;
    ColorAttribute.location = 2;
    ColorAttribute.format = VK_FORMAT_R8G8B8A8_UNORM;
    ColorAttribute.offset = offsetof(ImDrawVert, col);
    VertexInputAttributes.push_back(ColorAttribute);

    VkPipelineVertexInputStateCreateInfo VertexInputState{};
    VertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    VertexInputState.vertexBindingDescriptionCount = 1;
    VertexInputState.pVertexBindingDescriptions = &VertexInputBinding;
    VertexInputState.vertexAttributeDescriptionCount = static_cast<uint32_t>(VertexInputAttributes.size());
    VertexInputState.pVertexAttributeDescriptions = VertexInputAttributes.data();

    VkPushConstantRange PushConstRange{};
    PushConstRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    PushConstRange.size = sizeof(ImGuiCtx::PushConstBlock);
    PushConstRange.offset = 0;

    std::vector<std::tuple<VkDescriptorType, VkShaderStageFlagBits>> DescriptorBindings = {
        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT }};

    std::vector<std::tuple<const std::string&, VkShaderStageFlagBits>> Shaders;
    std::string VertexPath = "assets/shaders/ui_vert.spv";
    std::string FragmentPath = "assets/shaders/ui_frag.spv";
    Shaders.push_back({VertexPath, VK_SHADER_STAGE_VERTEX_BIT});
    Shaders.push_back({FragmentPath, VK_SHADER_STAGE_FRAGMENT_BIT});

    ImguiCtx->Mat = VkCtx->CreateMaterial(DescriptorBindings, Shaders, VertexInputState, PushConstRange, false);
    
    VkDescriptorImageInfo FontDescriptor{};
    FontDescriptor.sampler = ImguiCtx->Font.Sampler;
    FontDescriptor.imageView = ImguiCtx->Font.View;
    FontDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkWriteDescriptorSet WriteSet{};
    WriteSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    WriteSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    WriteSet.dstBinding = 0;
    WriteSet.pImageInfo = &FontDescriptor;
    WriteSet.descriptorCount = 1;

    std::vector<std::vector<VkWriteDescriptorSet>> WriteSets(VkCtx->GetNumFrames(), {WriteSet});
    ImguiCtx->MatInst = VkCtx->CreateMaterialInstance(ImguiCtx->Mat, WriteSets);
}

static void CreatePipeline(ImGuiCtx* ImguiCtx, const VulkanContext* VkCtx)
{
    ZoneScoped

    uint32_t NumFrames = VkCtx->GetNumFrames();
    VkCommandPoolCreateInfo CommandPoolInfo{};
    CommandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    CommandPoolInfo.queueFamilyIndex = VkCtx->GetGraphicsQueueFamily();
    CommandPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    vkCreateCommandPool(VkCtx->GetDevice(), &CommandPoolInfo, nullptr, &ImguiCtx->CommandPool);

    VkCommandBufferAllocateInfo CmdBuffAllocInfo{};
    CmdBuffAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    CmdBuffAllocInfo.commandPool = ImguiCtx->CommandPool;
    CmdBuffAllocInfo.commandBufferCount = NumFrames;
    CmdBuffAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;

    ImguiCtx->CommandBuffers.resize(NumFrames);
    // Allocate secondary command buffers
    vkAllocateCommandBuffers(VkCtx->GetDevice(), &CmdBuffAllocInfo, ImguiCtx->CommandBuffers.data());

    VkCommandBufferInheritanceInfo InheritanceInfo{};
    InheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    InheritanceInfo.renderPass = VkCtx->GetRenderPass();
    InheritanceInfo.subpass = ImguiCtx->Mat.Subpass;

    VkCommandBufferBeginInfo BeginInfo{};
    BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    BeginInfo.pInheritanceInfo = &InheritanceInfo;
    BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;

    ImguiCtx->PushConstBlocks.resize(NumFrames);
    // Just begin an empty command buffer for each one initially
    int i = 0;
    for (auto& CommandBuffer : ImguiCtx->CommandBuffers)
    {
        InheritanceInfo.framebuffer = VkCtx->GetFramebuffer(i);
        vkBeginCommandBuffer(CommandBuffer, &BeginInfo);
        ImguiCtx->Mat.Bind(CommandBuffer);
        ImguiCtx->MatInst.Bind(CommandBuffer, i, &ImguiCtx->PushConstBlocks[0]);
        vkEndCommandBuffer(CommandBuffer);
        i++;
    }

    ImguiCtx->VertexBuffers.resize(NumFrames);
    ImguiCtx->IndexBuffers.resize(NumFrames);
    ImguiCtx->VertexCounts.resize(NumFrames);
    ImguiCtx->IndexCounts.resize(NumFrames);
    ImguiCtx->PushConstBlocks.resize(NumFrames);
    VkCtx->RegisterUICommands(&ImguiCtx->CommandBuffers);
}

void ImGuiSubmit::initialize()
{
    ZoneScoped
    ImGuiCtx* ImguiCtx = get_singleton<ImGuiCtx>();
    const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();
    const Window* WindowPtr = get_singleton_const<Window>();

    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();        
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    io.DisplaySize = ImVec2((float)WindowPtr->windowWidth, (float)WindowPtr->windowHeight);

    ImGui_ImplGlfw_InitForVulkan(WindowPtr->window, true);

    ImGui::StyleColorsDark();
    ImGui::GetStyle().WindowRounding = 1.f;

    io.Fonts->AddFontFromFileTTF("assets/fonts/Roboto-Medium.ttf", 16.f);
    io.DisplaySize = ImVec2((float)INITIAL_WIDTH, (float)INITIAL_HEIGHT);

    CreateRenderingResources(ImguiCtx, VkCtx);
    CreatePipeline(ImguiCtx, VkCtx);

    ImGui_ImplVulkan_InitInfo InitInfo{};
    InitInfo.Device = VkCtx->GetDevice();
    InitInfo.PhysicalDevice = VkCtx->GetPhysicalDevice();
    InitInfo.PipelineCache = ImguiCtx->Mat.PipelineCache;
    InitInfo.MSAASamples = VkCtx->GetSampleCount();
    InitInfo.DescriptorPool = ImguiCtx->Mat.DescriptorPool;
    InitInfo.ImageCount = VkCtx->GetNumFrames();
    InitInfo.Instance = VkCtx->GetInstance();
    InitInfo.MinImageCount = VkCtx->GetNumFrames();
    InitInfo.QueueFamily = VkCtx->GetGraphicsQueueFamily();
    InitInfo.Queue = VkCtx->GetGraphicsQueue();
    InitInfo.Allocator = nullptr;
    ImGui_ImplVulkan_Init(&InitInfo, VkCtx->GetRenderPass());

    ImGui_ImplGlfw_NewFrame();
    ImGui_ImplVulkan_NewFrame();
    ImGui::NewFrame();
}

static bool RebuildVertexBuffer(ImGuiCtx* ImguiCtx, const VulkanContext* VkCtx, const Window* WindowPtr, uint32_t FrameIndex)
{
    ZoneScoped

    ImDrawData* imDrawData = ImGui::GetDrawData();
    
    bool bShouldRebuildCommands = true;

    if (!imDrawData) return false;

    VkDeviceSize VertexBufferSize = imDrawData->TotalVtxCount * sizeof(ImDrawVert);
    VkDeviceSize IndexBufferSize = imDrawData->TotalIdxCount * sizeof(ImDrawIdx);

    if ((VertexBufferSize == 0) || (IndexBufferSize == 0)) return false;

    if ((ImguiCtx->VertexBuffers[FrameIndex].Buffer == VK_NULL_HANDLE) || 
        (ImguiCtx->VertexCounts[FrameIndex] != imDrawData->TotalVtxCount))
    {
        if (ImguiCtx->VertexBuffers[FrameIndex].Buffer != VK_NULL_HANDLE)
        {
            vkDestroyBuffer(VkCtx->GetDevice(), ImguiCtx->VertexBuffers[FrameIndex].Buffer, nullptr);
            vkFreeMemory(VkCtx->GetDevice(), ImguiCtx->VertexBuffers[FrameIndex].Memory, nullptr);
        }
        ImguiCtx->VertexBuffers[FrameIndex] = VkCtx->CreateBuffer(VertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        ImguiCtx->VertexCounts[FrameIndex] = imDrawData->TotalVtxCount;
        bShouldRebuildCommands = true;
    }

    if ((ImguiCtx->IndexBuffers[FrameIndex].Buffer == VK_NULL_HANDLE) ||
        (ImguiCtx->IndexCounts[FrameIndex] != imDrawData->TotalIdxCount))
    {
        if (ImguiCtx->IndexBuffers[FrameIndex].Buffer != VK_NULL_HANDLE)
        {
            vkDestroyBuffer(VkCtx->GetDevice(), ImguiCtx->IndexBuffers[FrameIndex].Buffer, nullptr);
            vkFreeMemory(VkCtx->GetDevice(), ImguiCtx->IndexBuffers[FrameIndex].Memory, nullptr);
        }
        ImguiCtx->IndexBuffers[FrameIndex] = VkCtx->CreateBuffer(IndexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        ImguiCtx->IndexCounts[FrameIndex] = imDrawData->TotalIdxCount;
        bShouldRebuildCommands = true;
    }

    void* VtxDst;
    vkMapMemory(VkCtx->GetDevice(), ImguiCtx->VertexBuffers[FrameIndex].Memory, 0, VertexBufferSize, 0, &VtxDst);
    void* IdxDst;
    vkMapMemory(VkCtx->GetDevice(), ImguiCtx->IndexBuffers[FrameIndex].Memory, 0, IndexBufferSize, 0, &IdxDst);

    ImDrawVert* VertexDestination = (ImDrawVert*)VtxDst;
    ImDrawIdx* IndexDestination = (ImDrawIdx*)IdxDst;

    for (int n = 0; n < imDrawData->CmdListsCount; ++n)
    {
        const ImDrawList* CmdList = imDrawData->CmdLists[n];
        memcpy(VertexDestination, CmdList->VtxBuffer.Data, CmdList->VtxBuffer.Size * sizeof(ImDrawVert));
        memcpy(IndexDestination, CmdList->IdxBuffer.Data, CmdList->IdxBuffer.Size * sizeof(ImDrawIdx));
        VertexDestination += CmdList->VtxBuffer.Size;
        IndexDestination += CmdList->IdxBuffer.Size;
    }

    vkUnmapMemory(VkCtx->GetDevice(), ImguiCtx->VertexBuffers[FrameIndex].Memory);
    vkUnmapMemory(VkCtx->GetDevice(), ImguiCtx->IndexBuffers[FrameIndex].Memory);

    return bShouldRebuildCommands;
}

static void RebuildCommandBuffers(ImGuiCtx* ImguiCtx, const VulkanContext* VkCtx, uint32_t FrameIndex)
{
    ZoneScoped
    const ImGuiIO& io = ImGui::GetIO();
    ImDrawData* imDrawData = ImGui::GetDrawData();
    if (!imDrawData) return;
    
    VkCommandBuffer& CommandBuffer = ImguiCtx->CommandBuffers[FrameIndex];

    VkCommandBufferInheritanceInfo InheritanceInfo{};
    InheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    InheritanceInfo.renderPass = VkCtx->GetRenderPass();
    InheritanceInfo.framebuffer = VkCtx->GetActiveFramebuffer();
    InheritanceInfo.subpass = ImguiCtx->Mat.Subpass;

    VkCommandBufferBeginInfo BeginInfo{};
    BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    BeginInfo.pInheritanceInfo = &InheritanceInfo;
    BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;

    vkBeginCommandBuffer(CommandBuffer, &BeginInfo);

    ImguiCtx->PushConstBlocks[FrameIndex].Scale = glm::vec2(2.0f / io.DisplaySize.x, 2.f / io.DisplaySize.y);
    ImguiCtx->PushConstBlocks[FrameIndex].Translate = glm::vec2(-1.f);
   
    ImguiCtx->Mat.Bind(CommandBuffer);
    ImguiCtx->MatInst.Bind(CommandBuffer, FrameIndex, &ImguiCtx->PushConstBlocks[FrameIndex]);

    VkDeviceSize Offsets[1] = {0};
    vkCmdBindVertexBuffers(CommandBuffer, 0, 1, &ImguiCtx->VertexBuffers[FrameIndex].Buffer, Offsets);
    vkCmdBindIndexBuffer(CommandBuffer, ImguiCtx->IndexBuffers[FrameIndex].Buffer, 0, VK_INDEX_TYPE_UINT16);

    uint32_t VertexOffset = 0;
    uint32_t IndexOffset = 0;
    for (uint32_t i = 0; i < imDrawData->CmdListsCount; ++i)
    {
        const ImDrawList* CmdList = imDrawData->CmdLists[i];
        for (uint32_t j = 0; j < CmdList->CmdBuffer.Size; ++j)
        {
            const ImDrawCmd* Cmd = &CmdList->CmdBuffer[j];
            VkRect2D ScissorRect;
            ScissorRect.offset.x = std::max((int32_t)(Cmd->ClipRect.x), 0);
            ScissorRect.offset.y = std::max((int32_t)(Cmd->ClipRect.y), 0);
            ScissorRect.extent.width = (uint32_t)(Cmd->ClipRect.z - Cmd->ClipRect.x);
            ScissorRect.extent.height = (uint32_t)(Cmd->ClipRect.w - Cmd->ClipRect.y);
            vkCmdSetViewport(CommandBuffer, 0, 1, &VkCtx->GetViewport());
            vkCmdSetScissor(CommandBuffer, 0, 1, &ScissorRect);
            vkCmdDrawIndexed(CommandBuffer, Cmd->ElemCount, 1, IndexOffset, VertexOffset, 0);
            IndexOffset += Cmd->ElemCount;
        }
        VertexOffset += CmdList->VtxBuffer.Size;
    }

    vkEndCommandBuffer(CommandBuffer);
}

void ImGuiSubmit::update()
{
    ZoneScopedN("ImGuiSubmit")

    ImGuiCtx* ImguiCtx = get_singleton<ImGuiCtx>();
    const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();
    const Window* WindowPtr = get_singleton_const<Window>();
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize = ImVec2((float)WindowPtr->windowWidth, (float)WindowPtr->windowHeight);

    ImGui::Render();
    
    uint32_t FrameIndex = VkCtx->GetFrameIndex();

    bool bShouldRebuild = RebuildVertexBuffer(ImguiCtx, VkCtx, WindowPtr, FrameIndex);
    if (bShouldRebuild)
    {
        RebuildCommandBuffers(ImguiCtx, VkCtx, FrameIndex);
    }

    glfwPollEvents();
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void ImGuiSubmit::terminate()
{
    ZoneScoped
    ImGui::Render();
    ImGuiCtx* ImguiCtx = get_singleton<ImGuiCtx>();
    const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();
    const VkDevice& Device = VkCtx->GetDevice();

    vkDeviceWaitIdle(VkCtx->GetDevice());

    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();

    ImGui::DestroyContext();

    for (uint32_t i = 0; i < ImguiCtx->VertexBuffers.size(); ++i)
    {
        VkCtx->DestroyBuffer(ImguiCtx->VertexBuffers[i]);
        VkCtx->DestroyBuffer(ImguiCtx->IndexBuffers[i]);
    }
    
    vkFreeCommandBuffers(Device, ImguiCtx->CommandPool, static_cast<uint32_t>(ImguiCtx->CommandBuffers.size()), ImguiCtx->CommandBuffers.data());
    vkDestroyCommandPool(Device, ImguiCtx->CommandPool, nullptr);

    VkCtx->DestroyTexture(ImguiCtx->Font);
    VkCtx->DestroyMaterialInstance(ImguiCtx->MatInst);
    VkCtx->DestroyMaterial(ImguiCtx->Mat);
}