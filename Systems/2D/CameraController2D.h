#ifndef CAMERA_CONTROLLER_2D_H
#define CAMERA_CONTROLLER_2D_H

#include "ECS/System.h"
#include "Components/Camera.h"
#include "Components/Window.h"

#include <glm/gtc/matrix_transform.hpp>

class CameraController2D : public System<OrthoCamera, const Window>
{
public:
    void update() override final
    {
        ZoneScopedN("CameraController2D")
        const Window* WindowPtr = get_singleton_const<Window>();
        
        uint32_t width = WindowPtr->windowWidth;
        uint32_t height = WindowPtr->windowHeight;

        OrthoCamera* camera = get_singleton<OrthoCamera>();
        float zoom = camera->zoom;
        glm::vec3 pos = camera->pos;
        
        float left = width * zoom;
        float top = height * zoom;
        camera->Projection = glm::ortho(-left, left, -top, top, -1.f, 1.f);

        glm::vec3 front = glm::vec3(0.f, 0.f, 1.f);
        glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);

        camera->View = glm::mat4(1.f);

        camera->ViewProjection = camera->Projection * camera->View;

    }
};

#endif // CAMERA_CONTROLLER_2D_H