#ifndef SPRITE_UPDATE_H
#define SPRITE_UPDATE_H

#include "ECS/System.h"
#include "Components/Sprite.h"
#include "Components/Transform.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

class SpriteUpdate : public System<Sprite, const Transform>
{
public:
    void update() override final
    {
        ZoneScopedN("SpriteUpdate")
        for (auto& Proxy : get_proxies())
        {
            Sprite* SpritePtr = Proxy.get<Sprite>();
            const Transform* TransformPtr = Proxy.get<Transform>();
            
            // Create a model matrix from the pos/scale/rot
            glm::quat Quat = glm::quat(TransformPtr->Rotation);
            glm::mat4 Rotate = glm::toMat4(Quat);
            glm::mat4 Scale = glm::scale(glm::mat4(1.f), TransformPtr->Scale);
            glm::mat4 Translate = glm::translate(glm::mat4(1.f), TransformPtr->Pos);

            SpritePtr->Model = Translate * Rotate * Scale;
        }
    }
};

#endif // SPRITE_UPDATE_H