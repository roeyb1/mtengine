#ifndef DRAW_SPRITES_H
#define DRAW_SPRITES_H

#include "ECS/System.h"

#include "Components/Sprite.h"
#include "Components/Camera.h"

#include "Systems/Rendering/RenderingControl.h"
#include "Drivers/Vulkan/VulkanContext.h"

#include <glm/gtc/matrix_transform.hpp>

static std::vector<Vertex> SpriteVertices = {
    {{ 1.f,  1.f, 0.0f}, {1.f, 1.f, 1.f}, {1.f, 1.f}},
    {{ 1.f, -1.f, 0.0f}, {1.f, 1.f, 1.f}, {1.f, 0.f}},
    {{-1.f, -1.f, 0.0f}, {1.f, 1.f, 1.f}, {0.f, 0.f}},
    {{-1.f,  1.f, 0.0f}, {1.f, 1.f, 1.f}, {0.f, 1.f}}};

static std::vector<uint32_t> SpriteIndices = {
    0, 1, 3,
    1, 2, 3};
    
static std::vector<Buffer> SpriteCamUBOs = {};

static VkCommandPool SpriteCommandPool = VK_NULL_HANDLE;
static std::vector<VkCommandBuffer> SecondaryCmdBuffers = {};

static VertexArray SpriteVA;

static Material SpriteMaterial;
// One material instance per sprite texture
static std::unordered_map<std::string, MaterialInstance> MaterialInstances;

class DrawSprites : public System<Sprite, const OrthoCamera, const VulkanContext, const RenderingBegin, DrawFence>
{
public:
    void CreateSpriteMaterialInstance(Sprite* SpritePtr)
    {
        ZoneScoped
        const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();
        std::vector<std::vector<VkWriteDescriptorSet>> WriteSets(VkCtx->GetNumFrames());

        VkDescriptorBufferInfo BufferInfo{};
        BufferInfo.offset = 0;
        BufferInfo.range = sizeof(CameraUBO);

        SpritePtr->Tex = VkCtx->CreateTexture(SpritePtr->TexturePath, true);

        VkDescriptorImageInfo ImageInfo{};
        ImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        ImageInfo.imageView = SpritePtr->Tex.View;
        ImageInfo.sampler = SpritePtr->Tex.Sampler;

        for (uint32_t i = 0; i < VkCtx->GetNumFrames(); ++i)
        {
            BufferInfo.buffer = SpriteCamUBOs[i].Buffer;

            std::vector<VkWriteDescriptorSet> DescriptorWrites(2);
            DescriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            DescriptorWrites[0].dstBinding = 0;
            DescriptorWrites[0].dstArrayElement = 0;
            DescriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            DescriptorWrites[0].descriptorCount = 1;
            DescriptorWrites[0].pBufferInfo = &BufferInfo;
            DescriptorWrites[0].pImageInfo = nullptr;

            DescriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            DescriptorWrites[1].dstBinding = 1;
            DescriptorWrites[1].dstArrayElement = 0;
            DescriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            DescriptorWrites[1].descriptorCount = 1;
            DescriptorWrites[1].pBufferInfo = nullptr;
            DescriptorWrites[1].pImageInfo = &ImageInfo;

            WriteSets[i] = DescriptorWrites;
        }

        MaterialInstances[SpritePtr->TexturePath] = VkCtx->CreateMaterialInstance(SpriteMaterial, WriteSets);
    }

    void initialize() override final
    {
        ZoneScoped
        const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();

        for (uint32_t i = 0; i < VkCtx->GetNumFrames(); ++i)
        {
            SpriteCamUBOs.push_back(VkCtx->CreateBuffer(sizeof(CameraUBO), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT));
        }

        std::vector<std::tuple<VkDescriptorType, VkShaderStageFlagBits>> DescriptorBindings = {
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT }};
        std::vector<std::tuple<const std::string&, VkShaderStageFlagBits>> Shaders;
        std::string VertexPath = "assets/shaders/sprite_vert.spv";
        std::string FragmentPath = "assets/shaders/sprite_frag.spv";
        Shaders.push_back({VertexPath, VK_SHADER_STAGE_VERTEX_BIT});
        Shaders.push_back({FragmentPath, VK_SHADER_STAGE_FRAGMENT_BIT});

        VkPushConstantRange PushConstantRange{};
        PushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        PushConstantRange.size = sizeof(ModelPushConst);
        PushConstantRange.offset = 0;

        auto AttributeDescriptions = Vertex::GetAttributeDescriptions();
        auto BindingDescriptions = Vertex::GetBindingDescription();

        VkPipelineVertexInputStateCreateInfo VertexInputInfo{};
        VertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        VertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(AttributeDescriptions.size());
        VertexInputInfo.pVertexAttributeDescriptions = AttributeDescriptions.data();
        VertexInputInfo.vertexBindingDescriptionCount = 1;
        VertexInputInfo.pVertexBindingDescriptions = &BindingDescriptions; 

        SpriteMaterial = VkCtx->CreateMaterial(DescriptorBindings, Shaders, VertexInputInfo, PushConstantRange, true);

        SpriteVA.vb = VkCtx->CreateVertexBuffer(SpriteVertices);
        SpriteVA.ib = VkCtx->CreateIndexBuffer(SpriteIndices);
        SpriteVA.numIndices = static_cast<uint32_t>(SpriteIndices.size());

        VkCommandPoolCreateInfo PoolCreateInfo{};
        PoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        PoolCreateInfo.queueFamilyIndex = VkCtx->GetGraphicsQueueFamily();
        PoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

        vkCreateCommandPool(VkCtx->GetDevice(), &PoolCreateInfo, nullptr, &SpriteCommandPool);

        VkCommandBufferAllocateInfo AllocInfo{};
        AllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        AllocInfo.commandPool = SpriteCommandPool;
        AllocInfo.commandBufferCount = VkCtx->GetNumFrames();
        AllocInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;

        SecondaryCmdBuffers.resize(VkCtx->GetNumFrames());
        vkAllocateCommandBuffers(VkCtx->GetDevice(), &AllocInfo, SecondaryCmdBuffers.data());

        VkCommandBufferInheritanceInfo InheritanceInfo{};
        InheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
        InheritanceInfo.renderPass = VkCtx->GetRenderPass();
        // This will be set inside the loop for each framebuffer index
        InheritanceInfo.framebuffer = VK_NULL_HANDLE;
        InheritanceInfo.subpass = SpriteMaterial.Subpass;

        VkCommandBufferBeginInfo BeginInfo{};
        BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        BeginInfo.pInheritanceInfo = &InheritanceInfo;
        BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
        int i = 0;
        for (VkCommandBuffer& CommandBuffer : SecondaryCmdBuffers)
        {
            InheritanceInfo.framebuffer = VkCtx->GetFramebuffer(i++);
            vkBeginCommandBuffer(CommandBuffer, &BeginInfo);
            SpriteMaterial.Bind(CommandBuffer);
            vkEndCommandBuffer(CommandBuffer);   
        }
        VkCtx->RegisterSecondaryCommands(&SecondaryCmdBuffers);
    }

    void update() override final
    {
        ZoneScopedN("DrawSprites")
        const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();
        uint32_t FrameIndex = VkCtx->GetFrameIndex();
        const OrthoCamera* Camera = get_singleton_const<OrthoCamera>();

        CameraUBO CamUBO{};
        CamUBO.View = Camera->View;
        CamUBO.Projection = Camera->Projection;

        void* data;
        vkMapMemory(VkCtx->GetDevice(), SpriteCamUBOs[FrameIndex].Memory, 0, sizeof(CameraUBO), 0, &data);
        memcpy(data, &CamUBO, sizeof(CameraUBO));
        vkUnmapMemory(VkCtx->GetDevice(), SpriteCamUBOs[FrameIndex].Memory);

        VkCommandBufferInheritanceInfo InheritanceInfo{};
        InheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
        InheritanceInfo.renderPass = VkCtx->GetRenderPass();
        InheritanceInfo.framebuffer = VkCtx->GetActiveFramebuffer();
        InheritanceInfo.subpass = SpriteMaterial.Subpass;

        VkCommandBufferBeginInfo BeginInfo{};
        BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        BeginInfo.pInheritanceInfo = &InheritanceInfo;
        BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
        
        VkCommandBuffer& CommandBuffer = SecondaryCmdBuffers[FrameIndex];
        vkBeginCommandBuffer(CommandBuffer, &BeginInfo);

        vkCmdSetViewport(CommandBuffer, 0, 1, &VkCtx->GetViewport());
        vkCmdSetScissor(CommandBuffer, 0, 1, &VkCtx->GetScissor());

        SpriteMaterial.Bind(CommandBuffer);

        VkDeviceSize Offsets[] = {0};
        vkCmdBindVertexBuffers(CommandBuffer, 0, 1, &SpriteVA.vb.Buffer, Offsets);
        vkCmdBindIndexBuffer(CommandBuffer, SpriteVA.ib.Buffer, 0, VK_INDEX_TYPE_UINT32);

        for (auto& Proxy : get_partial_proxy<Sprite>())
        {
            Sprite* SpritePtr = Proxy.get<Sprite>();
            if (!MaterialInstances.contains(SpritePtr->TexturePath))
            {
                CreateSpriteMaterialInstance(SpritePtr);
            }
            MaterialInstances[SpritePtr->TexturePath].Bind(CommandBuffer, FrameIndex, &SpritePtr->Model);
            vkCmdDrawIndexed(CommandBuffer, SpriteVA.numIndices, 1, 0, 0, 0);
        }
        vkEndCommandBuffer(CommandBuffer);
    }

    void terminate() override final
    {
        const VulkanContext* VkCtx = get_singleton_const<VulkanContext>();

        vkDeviceWaitIdle(VkCtx->GetDevice());
        
        for (auto& Proxy : get_partial_proxy<Sprite>())
        {
            Sprite* SpritePtr = Proxy.get<Sprite>();
            VkCtx->DestroyTexture(SpritePtr->Tex);
        }

        for (auto& [TexPath, MatInst] : MaterialInstances)
        {
            VkCtx->DestroyMaterialInstance(MatInst);
        }

        vkFreeCommandBuffers(VkCtx->GetDevice(), SpriteCommandPool, static_cast<uint32_t>(SecondaryCmdBuffers.size()), SecondaryCmdBuffers.data());
        vkDestroyCommandPool(VkCtx->GetDevice(), SpriteCommandPool, nullptr);

        for (Buffer& Buff : SpriteCamUBOs)
        {
            VkCtx->DestroyBuffer(Buff);
        }

        VkCtx->DestroyBuffer(SpriteVA.vb);
        VkCtx->DestroyBuffer(SpriteVA.ib);
        VkCtx->DestroyMaterial(SpriteMaterial);
    }

};

#endif // DRAW_SPRITES_H