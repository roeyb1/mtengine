#ifndef TRANSFORM_COMPONENT_H
#define TRANSFORM_COMPONENT_H

#include "ECS/Component.h"

#include <glm/glm.hpp>

struct Transform
{
    COMPONENT_DEFINITION(Transform);
    glm::vec3 Pos;
    glm::vec3 Rotation;
    glm::vec3 Scale;
};

#endif // TRANSFORM_COMPONENT_H