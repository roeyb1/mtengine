#ifndef SPRITE_COMPONENT_H
#define SPRITE_COMPONENT_H

#include "ECS/Component.h"
#include "core/rid.h"

#include "Drivers/Vulkan/VulkanTypes.h"

#include <glm/glm.hpp>

struct Sprite
{
    COMPONENT_DEFINITION(Sprite);

    glm::vec3 Color;

    glm::mat4 Model;

    std::string TexturePath = "assets/textures/default.png";
    Texture Tex;
};

#endif // SPRITE_COMPONENT_H