#ifndef TIME_SINGLETON_H
#define TIME_SINGLETON_H

#include "ECS/Component.h"

#include <chrono>

struct Time
{
    SINGLETON_COMPONENT(Time);
    double deltaTime = 0;
    // Time since application start in seconds
    double Uptime = 0;
    std::chrono::_V2::high_resolution_clock::time_point lastTime;
    std::chrono::_V2::high_resolution_clock::time_point currentTime;
};

#endif // TIME_SINGLETON_H