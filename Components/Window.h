#ifndef WINDOW_SINGLETON_COMPONENT_H
#define WINDOW_SINGLETON_COMPONENT_H

#include "ECS/Component.h"

#define INITIAL_WIDTH 1280
#define INITIAL_HEIGHT 720

struct GLFWwindow;

struct Window
{
    SINGLETON_COMPONENT(Window);
    uint32_t windowWidth, windowHeight;
    GLFWwindow* window = nullptr;
    bool bHasResizeEvent = true;
};

#endif // WINDOW_SINGLETON_COMPONET_H