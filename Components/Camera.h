#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include "ECS/Component.h"
#include <glm/glm.hpp>

struct OrthoCamera
{
    SINGLETON_COMPONENT(OrthoCamera);
    glm::mat4 Projection;
    glm::mat4 View;

    glm::mat4 ViewProjection;

    glm::vec3 pos = {0, 0, 0};
    
    float zoom = 1.f;
};

struct PerspCamera
{
    SINGLETON_COMPONENT(PerspCamera);
    glm::mat4 projection;
    glm::mat4 view;

    glm::mat4 view_projection;

    glm::vec3 pos = {0, 0, 0};

    float zoom = 1.f;
};

#endif // CAMERA_COMPONENT_H