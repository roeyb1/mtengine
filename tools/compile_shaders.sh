#!/bin/bash

glslc assets/shaders/vulkan_shader.vert -o assets/shaders/vert.spv
glslc assets/shaders/vulkan_shader.frag -o assets/shaders/frag.spv

# UI shaders
glslc assets/shaders/ui.vert -o assets/shaders/ui_vert.spv
glslc assets/shaders/ui.frag -o assets/shaders/ui_frag.spv