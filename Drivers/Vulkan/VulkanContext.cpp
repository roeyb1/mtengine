#include "VulkanContext.h"

#include "Components/Window.h"

#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <array>
#include <sstream>
#include <set>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

#include "VulkanTools.h"

const std::vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation" };

const std::vector<const char*> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME };

#ifndef NDEBUG
    constexpr bool enableValidationLayers = true;
#else
    constexpr bool enableValidationLayers = false;
#endif

// Number of frames that can be worked on at a time
constexpr int MAX_FRAMES_IN_FLIGHT = 2;

static std::vector<char> read_file(const std::string& filename)
{   
    ZoneScoped
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if (!file.is_open())
    {
        std::cout << "ERROR: Failed to open file: " << filename << std::endl;
    }

    size_t fileSize = (size_t)file.tellg();
    std::vector<char> buffer(fileSize);
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    return buffer;
}

VKAPI_ATTR VkBool32 VKAPI_CALL debugUtilsMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    // Select prefix depending on flags passed to the callback
    std::string prefix("");

    if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
        prefix = "VERBOSE: ";
    else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
        prefix = "INFO: ";
    else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        prefix = "WARNING: ";
    else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        prefix = "ERROR: ";

    std::stringstream debugMessage;
    debugMessage << prefix << "[" << pCallbackData->messageIdNumber << "][" << pCallbackData->pMessageIdName << "] : " << pCallbackData->pMessage;

    if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        std::cerr << debugMessage.str() << "\n";
    else
        std::cout << debugMessage.str() << "\n";

    fflush(stdout);

    return VK_FALSE;
}

static VkResult CreateDebugUtilsMessengerEXT(
    VkInstance instance, 
    const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
    const VkAllocationCallbacks* pAllocator, 
    VkDebugUtilsMessengerEXT* pDebugMessenger) 
{
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) 
    {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else 
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

static void DestroyDebugUtilsMessengerEXT(
    VkInstance instance, 
    VkDebugUtilsMessengerEXT debugMessenger, 
    const VkAllocationCallbacks* pAllocator) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) 
    {
        func(instance, debugMessenger, pAllocator);
    }
}

void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = debugUtilsMessengerCallback;
}

static bool check_validation_layer_support()
{
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (const char* name : validationLayers)
    {
        bool found = false;
        for (const auto& layerProperties : availableLayers)
        {
            if (strcmp(name, layerProperties.layerName) == 0)
            {
                found = true;
                break;
            }
        }

        if (!found) return false;
    }

    return true;
}

VulkanContext::QueueFamilyIndices VulkanContext::FindQueueFamilies(VkPhysicalDevice device) const
{
    ZoneScoped
    QueueFamilyIndices indices;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    for (const auto& queueFamily : queueFamilies)
    {
        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, Surface, &presentSupport);
        if (presentSupport)
            indices.PresentFamily = i;

        if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            indices.GraphicsFamily = i;
        }
        if (indices.FoundAll())
            break;
        i++;
    }

    return indices;
}

static bool check_device_extension_support(VkPhysicalDevice device)
{
    ZoneScoped
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

    std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

    for (const auto& extension : availableExtensions)
    {
        requiredExtensions.erase(extension.extensionName);
    }
    return requiredExtensions.empty();
}

bool VulkanContext::CheckPhysicalDeviceSuitability(VkPhysicalDevice device)
{
    ZoneScoped
    QueueFamilyIndices indices = FindQueueFamilies(device);
    bool extensionsSupported = check_device_extension_support(device);

    bool swapchainAdequate = false;
    if (extensionsSupported)
    {
        SwapchainSupportDetails swapchainSupport = QuerySwapchainSupport(device);

        VkPhysicalDeviceFeatures supportedFeatures;
        vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

        swapchainAdequate = !swapchainSupport.Formats.empty() && !swapchainSupport.PresentModes.empty() && supportedFeatures.samplerAnisotropy;
    }

    return indices.FoundAll() && extensionsSupported && swapchainAdequate;
}

static std::vector<const char*> get_required_extensions()
{
    ZoneScoped
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
    if (enableValidationLayers)
    {
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }
    return extensions;
}

VulkanContext::SwapchainSupportDetails VulkanContext::QuerySwapchainSupport(VkPhysicalDevice device)
{
    ZoneScoped
    SwapchainSupportDetails details;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, Surface, &details.Capabilities);

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, Surface, &formatCount, nullptr);
    if (formatCount != 0)
    {
        details.Formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, Surface, &formatCount, details.Formats.data());
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, Surface, &presentModeCount, nullptr);
    if (presentModeCount != 0)
    {
        details.PresentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, Surface, &presentModeCount, details.PresentModes.data());
    }
    
    return details;
}

VkSurfaceFormatKHR VulkanContext::ChooseSwapchainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
    ZoneScoped
    for (const auto& availableFormat : availableFormats)
    {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && 
            availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            return availableFormat;
        }
    }

    // fallback on the first format
    return availableFormats[0];
}

VkPresentModeKHR VulkanContext::ChooseSwapchainPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
{
    ZoneScoped
#ifndef NDEBUG
    // If in debug mode, render immediately so frametimes aren't artificially inflated waiting for v-blank
    return VK_PRESENT_MODE_IMMEDIATE_KHR;
#endif
    // Try to find MAILBOX if available, since it is preferred.
    for (const auto& availablePresentMode : availablePresentModes)
    {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
            return availablePresentMode;
    }
    // Failback on FIFO (basically vSync)
    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D VulkanContext::ChooseSwapchainExtent(const VkSurfaceCapabilitiesKHR& capabilities)
{
    ZoneScoped
    if (capabilities.currentExtent.width != UINT32_MAX)
    {
        return capabilities.currentExtent;
    }
    else
    {
        int width, height;
        glfwGetFramebufferSize(WindowPtr->window, &width, &height);

        VkExtent2D actualExtent = {
            static_cast<uint32_t>(width), 
            static_cast<uint32_t>(height)};
        // clamp between actual width/height
        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

VkFormat VulkanContext::FindSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
    for (VkFormat format : candidates)
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(PhysicalDevice, format, &props);
        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
            return format;
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
            return format;
    }
    std::cout << "FATAL: Failed to find a supported format!" << std::endl;
    return VK_FORMAT_END_RANGE;
}

VkFormat VulkanContext::FindCompatibleDepthFormat()
{
    return FindSupportedFormat({VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT}, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

bool VulkanContext::HasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkSampleCountFlagBits VulkanContext::GetMaxUsableSampleCount()
{
    VkPhysicalDeviceProperties physicalDeviceProperties;
    vkGetPhysicalDeviceProperties(PhysicalDevice, &physicalDeviceProperties);
    
    VkSampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts & physicalDeviceProperties.limits.framebufferColorSampleCounts;
    // Clamp to 16 samples
    if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
    if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
    if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
    if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
    if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
    if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }

    return VK_SAMPLE_COUNT_1_BIT;
}

VkCommandBuffer VulkanContext::BeginOneTimeCommands() const
{
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = PrimaryCommandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(Device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);
    return commandBuffer;
}

void VulkanContext::EndOneTimeCommands(VkCommandBuffer commandBuffer) const
{
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(Queues.Graphics, 1 , &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(Queues.Graphics);

    vkFreeCommandBuffers(Device, PrimaryCommandPool, 1, &commandBuffer);
}

void VulkanContext::TransitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels) const
{
    VkCommandBuffer commandBuffer = BeginOneTimeCommands();

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = mipLevels;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
        std::cout << "FATAL: Unsupported layout transition!" << std::endl;
    }
    

    vkCmdPipelineBarrier(
        commandBuffer,
        sourceStage, destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier);

    EndOneTimeCommands(commandBuffer);
}

void VulkanContext::CopyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) const
{
    VkCommandBuffer commandBuffer = BeginOneTimeCommands();

    VkBufferImageCopy region{};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = { 0, 0, 0 };
    region.imageExtent = { width, height, 1};

    vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    EndOneTimeCommands(commandBuffer);
}

void VulkanContext::GenerateMipmaps(VkImage image, VkFormat format, int32_t texWidth, int32_t texHeight, uint32_t mipLevels) const
{
    VkFormatProperties formatProperties;
    vkGetPhysicalDeviceFormatProperties(PhysicalDevice, format, &formatProperties);
    if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT))
    {
        std::cout << "FATAL: Texture image format does not support linear blitting!" << std::endl;
        return;
    }
    VkCommandBuffer commandBuffer = BeginOneTimeCommands();
    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.image = image;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.subresourceRange.levelCount = 1;

    int32_t mipWidth = texWidth;
    int32_t mipHeight = texHeight;

    for (uint32_t i = 1; i < mipLevels; ++i)
    {
        barrier.subresourceRange.baseMipLevel = i - 1;
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

        vkCmdPipelineBarrier(commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
            0, nullptr,
            0, nullptr,
            1, &barrier);

        VkImageBlit blit{};
        blit.srcOffsets[0] = {0, 0, 0};
        blit.srcOffsets[1] = {mipWidth, mipHeight, 1};
        blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.srcSubresource.mipLevel = i - 1;
        blit.srcSubresource.baseArrayLayer = 0;
        blit.srcSubresource.layerCount = 1;
        blit.dstOffsets[0] = {0, 0, 0};
        blit.dstOffsets[1] = {mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1};
        blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.dstSubresource.mipLevel = i;
        blit.dstSubresource.baseArrayLayer = 0;
        blit.dstSubresource.layerCount = 1;

        vkCmdBlitImage(commandBuffer,
            image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1, &blit, VK_FILTER_LINEAR);

        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, nullptr,
            0, nullptr,
            1, &barrier);

        if (mipWidth > 1) mipWidth /= 2;
        if (mipHeight > 1) mipHeight /= 2;
    }

    barrier.subresourceRange.baseMipLevel = mipLevels - 1;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    vkCmdPipelineBarrier(commandBuffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
        0, nullptr,
        0, nullptr,
        1, &barrier);

    EndOneTimeCommands(commandBuffer);
}

void VulkanContext::CreateInstance()
{
    ZoneScoped
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "MTEngine";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "MTEngine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    auto extensions = get_required_extensions();
    
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();
    
    createInfo.enabledLayerCount = 0;
    createInfo.pNext = nullptr;

    if (enableValidationLayers)
    {
        if (check_validation_layer_support())
        {
            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();
            
            VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo{};
            populateDebugMessengerCreateInfo(debugCreateInfo);
            createInfo.pNext = &debugCreateInfo;
        }
        else
        {
            std::cout << "WARN: Validation layers requested but not available!" << std::endl;
        }
    }

    VK_CHECK_RESULT(vkCreateInstance(&createInfo, nullptr, &Instance));
}

void VulkanContext::SetupDebugMessenger()
{
    ZoneScoped
    if (!enableValidationLayers) return;

    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
    populateDebugMessengerCreateInfo(createInfo);

    VK_CHECK_RESULT(CreateDebugUtilsMessengerEXT(Instance, &createInfo, nullptr, &DebugMessenger));
}

void VulkanContext::CreateSurface()
{
    ZoneScoped
    VK_CHECK_RESULT(glfwCreateWindowSurface(Instance, WindowPtr->window, nullptr, &Surface));
}

void VulkanContext::PickPhysicalDevice()
{
    ZoneScoped
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(Instance, &deviceCount, nullptr);
    if (deviceCount == 0)
    {
        std::cout << "FATAL: Failed to find a GPU with Vulkan support!" << std::endl;
        return;
    }

    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(Instance, &deviceCount, devices.data());

    for (const auto& device : devices)
    {
        if (CheckPhysicalDeviceSuitability(device))
        {
            PhysicalDevice = device;
            if (GetMaxUsableSampleCount() >= VK_SAMPLE_COUNT_16_BIT)
            {
                MSAASampleCount = VK_SAMPLE_COUNT_16_BIT;
            }
            else
            {
                MSAASampleCount = GetMaxUsableSampleCount();
            }
            break;
        }
    }

    if (PhysicalDevice == VK_NULL_HANDLE)
    {
        std::cout << "FATAL: Failed to find a suitable GPU!" << std::endl;
        return;
    }
}

void VulkanContext::CreateLogicalDevice()
{
    ZoneScoped
    QueueFamilyIndices indices = FindQueueFamilies(PhysicalDevice);

    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    deviceFeatures.sampleRateShading = VK_TRUE;

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = {indices.GraphicsFamily.value(), indices.PresentFamily.value()};

    float queuePriority = 1.f;
    for (uint32_t queueFamily : uniqueQueueFamilies)
    {
        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos = queueCreateInfos.data();

    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions.data();
    // Validation layers are no longer required to be set in logical device create
    // but for backwards compatability, just set them here anyways.
    if (enableValidationLayers)
    {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }
    else
    {
        createInfo.enabledLayerCount = 0;
    }

    VK_CHECK_RESULT(vkCreateDevice(PhysicalDevice, &createInfo, nullptr, &Device));

    vkGetDeviceQueue(Device, indices.GraphicsFamily.value(), 0, &Queues.Graphics);
    vkGetDeviceQueue(Device, indices.PresentFamily.value(), 0, &Queues.Present);
}

void VulkanContext::CreateSwapchain()
{
    ZoneScoped
    SwapchainSupportDetails swapchainSupport = QuerySwapchainSupport(PhysicalDevice);

    VkSurfaceFormatKHR surfaceFormat = ChooseSwapchainSurfaceFormat(swapchainSupport.Formats);
    VkPresentModeKHR presentMode = ChooseSwapchainPresentMode(swapchainSupport.PresentModes);
    VkExtent2D extent = ChooseSwapchainExtent(swapchainSupport.Capabilities);

    uint32_t imageCount = swapchainSupport.Capabilities.minImageCount + 1;
    // clamp between minImage + 1 and maxImage
    if (swapchainSupport.Capabilities.maxImageCount > 0 && imageCount > swapchainSupport.Capabilities.maxImageCount)
    {
        imageCount == swapchainSupport.Capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = Surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    QueueFamilyIndices indices = FindQueueFamilies(PhysicalDevice);
    uint32_t queueFamilyIndices[] = {indices.GraphicsFamily.value(), indices.PresentFamily.value()};
    if (indices.GraphicsFamily != indices.PresentFamily)
    {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }
    else
    {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }
    createInfo.preTransform = swapchainSupport.Capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    VK_CHECK_RESULT(vkCreateSwapchainKHR(Device, &createInfo, nullptr, &Swapchain));

    vkGetSwapchainImagesKHR(Device, Swapchain, &imageCount, nullptr);
    SwapchainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(Device, Swapchain, &imageCount, SwapchainImages.data());

    SwapchainImageFormat = surfaceFormat.format;
    SwapchainExtent = extent;

    WindowPtr->windowWidth = extent.width;
    WindowPtr->windowHeight = extent.height;
    WindowPtr->bHasResizeEvent = true;

    Viewport.x = 0.f;
    Viewport.y = 0.f;
    Viewport.width = static_cast<float>(extent.width);
    Viewport.height = static_cast<float>(extent.height);
    Viewport.minDepth = 0.f;
    Viewport.maxDepth = 1.f;

    Scissor.extent = extent;
    Scissor.offset = {0, 0};
}

void VulkanContext::CreateSwapchainViews()
{
    ZoneScoped
    SwapchainImageViews.resize(SwapchainImages.size());

    for (size_t i = 0; i < SwapchainImages.size(); ++i)
    {
        SwapchainImageViews[i] = CreateImageView(SwapchainImages[i], SwapchainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);      
    }
}

VkShaderModule VulkanContext::CreateShaderModule(const std::vector<char>& code) const
{
    ZoneScoped
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = code.size();
    createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
    VkShaderModule shaderModule;
    VK_CHECK_RESULT(vkCreateShaderModule(Device, &createInfo, nullptr, &shaderModule));
    return shaderModule;
}

VkPipelineShaderStageCreateInfo VulkanContext::CreateShader(const std::string& Path, VkShaderStageFlagBits Stage) const
{
    std::vector<char> Code = read_file(Path);

    VkShaderModule Module = CreateShaderModule(Code);
    VkPipelineShaderStageCreateInfo ShaderStageInfo{};
    ShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    ShaderStageInfo.stage = Stage;
    ShaderStageInfo.module = Module;
    ShaderStageInfo.pName = "main";

    return ShaderStageInfo;
}

void VulkanContext::RegisterSecondaryCommands(const std::vector<VkCommandBuffer>* SecondaryCommands) const
{
    std::lock_guard<std::mutex> Lock(SecondaryCmdMutex);
    ExternalSecondaryBuffers.push_back(SecondaryCommands);
}

void VulkanContext::RegisterUICommands(const std::vector<VkCommandBuffer>* UICommands) const
{
    UICommandBuffers = UICommands;
}

void VulkanContext::CreateRenderPass()
{
    ZoneScoped
    VkAttachmentDescription colorAttachment{};
    colorAttachment.format = SwapchainImageFormat;
    colorAttachment.samples = MSAASampleCount;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription colorAttachmentResolve{};
    colorAttachmentResolve.format = SwapchainImageFormat;
    colorAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentDescription depthAttachment{};
    depthAttachment.format = FindCompatibleDepthFormat();
    depthAttachment.samples = MSAASampleCount;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference colorAttachmentRef{};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef{};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference colorAttachmentResolveRef{};
    colorAttachmentResolveRef.attachment = 2;
    colorAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
    subpass.pResolveAttachments = &colorAttachmentResolveRef;

    VkSubpassDependency dependency{};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    std::array<VkAttachmentDescription, 3> attachments = {colorAttachment, depthAttachment, colorAttachmentResolve};

    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;    
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    VK_CHECK_RESULT(vkCreateRenderPass(Device, &renderPassInfo, nullptr, &RenderPass));
}

void VulkanContext::CreateFramebuffers()
{
    ZoneScoped
    SwapchainFramebuffers.resize(SwapchainImageViews.size());

    for (size_t i = 0; i < SwapchainImageViews.size(); ++i)
    {
        std::array<VkImageView, 3> attachments = {
            ColorImageView,
            DepthImageView,
            SwapchainImageViews[i]};
        
        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = RenderPass;
        framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        framebufferInfo.pAttachments = attachments.data();
        framebufferInfo.width = SwapchainExtent.width;
        framebufferInfo.height = SwapchainExtent.height;
        framebufferInfo.layers = 1;

        VK_CHECK_RESULT(vkCreateFramebuffer(Device, &framebufferInfo, nullptr, &SwapchainFramebuffers[i]));
    }
}

void VulkanContext::CreateCommandPool()
{
    ZoneScoped
    QueueFamilyIndices QueueFamilyIndices = FindQueueFamilies(PhysicalDevice);

    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = QueueFamilyIndices.GraphicsFamily.value();
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VK_CHECK_RESULT(vkCreateCommandPool(Device, &poolInfo, nullptr, &PrimaryCommandPool));
}

uint32_t VulkanContext::FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const
{
    ZoneScoped
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(PhysicalDevice, &memProperties);
    
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) 
    {
        if ((typeFilter & (1 << i)) && 
            (memProperties.memoryTypes[i].propertyFlags & properties) == properties) 
        {
            return i;
        }
    }

    std::cout << "FATAL: Failed to find a suitable memory type!" << std::endl;
    return 0;
}

void VulkanContext::CreateColorResources()
{
    VkFormat colorFormat = SwapchainImageFormat;

    CreateImage(SwapchainExtent.width, SwapchainExtent.height, 1, MSAASampleCount, colorFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, ColorImage, ColorImageMemory);
    ColorImageView = CreateImageView(ColorImage, colorFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

void VulkanContext::CreateDepthResources()
{
    VkFormat depthFormat = FindCompatibleDepthFormat();

    CreateImage(SwapchainExtent.width, SwapchainExtent.height, 1, MSAASampleCount, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, DepthImage, DepthImageMemory);
    DepthImageView = CreateImageView(DepthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);


}

Buffer VulkanContext::CreateBuffer(
    VkDeviceSize Size, VkBufferUsageFlags Usage,
    VkMemoryPropertyFlags Properties) const
{
    Buffer Buffer;
    AllocateBuffer(Size, Usage, Properties, Buffer.Buffer, Buffer.Memory);
    return Buffer;
}

void VulkanContext::DestroyBuffer(Buffer& Buffer) const
{
    vkDestroyBuffer(Device, Buffer.Buffer, nullptr);
    vkFreeMemory(Device, Buffer.Memory, nullptr);
    Buffer.Buffer = VK_NULL_HANDLE;
    Buffer.Memory = VK_NULL_HANDLE;
}

void VulkanContext::AllocateBuffer(VkDeviceSize size, 
    VkBufferUsageFlags usage, 
    VkMemoryPropertyFlags properties, 
    VkBuffer& buffer, 
    VkDeviceMemory& bufferMemory) const
{
    ZoneScoped
    VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    
    VK_CHECK_RESULT(vkCreateBuffer(Device, &bufferInfo, nullptr, &buffer));
    
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(Device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = FindMemoryType(memRequirements.memoryTypeBits, properties);

    VK_CHECK_RESULT(vkAllocateMemory(Device, &allocInfo, nullptr, &bufferMemory));

    vkBindBufferMemory(Device, buffer, bufferMemory, 0);
}

void VulkanContext::CopyBufferToBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) const
{
    ZoneScoped
    VkCommandBuffer commandBuffer = BeginOneTimeCommands();

    VkBufferCopy copyRegion{};
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = 0;
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    EndOneTimeCommands(commandBuffer);
}

Buffer VulkanContext::CreateVertexBuffer(const std::vector<Vertex>& vertices) const
{
    ZoneScoped
    VkDeviceSize bufferSize = sizeof(Vertex) * vertices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    AllocateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
    void* data;
    vkMapMemory(Device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, vertices.data(), (size_t)bufferSize);
    vkUnmapMemory(Device, stagingBufferMemory);

    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    AllocateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);
    CopyBufferToBuffer(stagingBuffer, vertexBuffer, bufferSize);
    vkDestroyBuffer(Device, stagingBuffer, nullptr);
    vkFreeMemory(Device, stagingBufferMemory, nullptr);

    return {vertexBuffer, vertexBufferMemory};
}

Buffer VulkanContext::CreateIndexBuffer(const std::vector<uint32_t>& indices) const
{
    ZoneScoped
    VkDeviceSize bufferSize = sizeof(uint32_t) * indices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    AllocateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

    void* data;
    vkMapMemory(Device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, indices.data(), (size_t) bufferSize);
    vkUnmapMemory(Device, stagingBufferMemory);

    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;

    AllocateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

    CopyBufferToBuffer(stagingBuffer, indexBuffer, bufferSize);

    vkDestroyBuffer(Device, stagingBuffer, nullptr);
    vkFreeMemory(Device, stagingBufferMemory, nullptr);

    Buffer Result;
    Result.Buffer = indexBuffer;
    Result.Memory = indexBufferMemory;

    return Result;
}

Texture VulkanContext::CreateTexture(
    const std::string& Path, bool bShouldCreateMips,
    VkFormat Format, VkImageTiling Tiling) const
{
    int Width, Height, NChannels;
    stbi_uc* PixelData = stbi_load(Path.c_str(), &Width, &Height, &NChannels, STBI_rgb_alpha);
    if (!PixelData)
    {
        std::cout << "ERROR: Failed to load image: " << Path << std::endl;
    }
    Texture Result = CreateTexture(PixelData, Width, Height, bShouldCreateMips, Format, Tiling);
    stbi_image_free(PixelData);
    return Result;
}

Texture VulkanContext::CreateTexture(
    unsigned char* PixelData, uint32_t Width, uint32_t Height, bool bShouldCreateMips,
    VkFormat Format, VkImageTiling Tiling) const
{
    VkDeviceSize ImageSize = Width * Height * 4;
    Texture Tex;

    Tex.MipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(Width, Height)))) + 1;
    if (!bShouldCreateMips)
    {
        Tex.MipLevels = 1;
    }

    VkBuffer StagingBuffer;
    VkDeviceMemory StagingBufferMemory;

    AllocateBuffer(
        ImageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
        StagingBuffer, StagingBufferMemory);

    // Load the pixel data into a staging buffer
    void* BufferData;
    vkMapMemory(Device, StagingBufferMemory, 0, ImageSize, 0, &BufferData);
    memcpy(BufferData, PixelData, static_cast<size_t>(ImageSize));
    vkUnmapMemory(Device, StagingBufferMemory);

    // Create the texture image
    // Default to using ImageSampled unless mipmaps need to be created
    VkImageUsageFlags ImageUsage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    VkMemoryPropertyFlags MemoryProperties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    // It will need to be a transfer source
    if (bShouldCreateMips)
    {
        ImageUsage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    }
    
    CreateImage(Width, Height, Tex.MipLevels, VK_SAMPLE_COUNT_1_BIT, Format, Tiling, ImageUsage, MemoryProperties, Tex.Image, Tex.Memory);
    TransitionImageLayout(Tex.Image, Format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, Tex.MipLevels);
    CopyBufferToImage(StagingBuffer, Tex.Image, static_cast<uint32_t>(Width), static_cast<uint32_t>(Height));

    // Destroy staging buffers
    vkDestroyBuffer(Device, StagingBuffer, nullptr);
    vkFreeMemory(Device, StagingBufferMemory, nullptr);

    if (bShouldCreateMips)
    {
        GenerateMipmaps(Tex.Image, Format, Width, Height, Tex.MipLevels);
    }
    else
    {
        // Since the mip generation will handle the final transition, and this image is not being blitted, transition manually here
        TransitionImageLayout(Tex.Image, Format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, Tex.MipLevels);
    }

    Tex.View = CreateImageView(Tex.Image, Format, VK_IMAGE_ASPECT_COLOR_BIT, Tex.MipLevels);
    
    VkSamplerCreateInfo SamplerInfo{};
    SamplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    SamplerInfo.magFilter = VK_FILTER_LINEAR;
    SamplerInfo.minFilter = VK_FILTER_LINEAR;
    SamplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    SamplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    SamplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    SamplerInfo.anisotropyEnable = VK_TRUE;
    SamplerInfo.maxAnisotropy = 16;
    SamplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    SamplerInfo.unnormalizedCoordinates = VK_FALSE;
    SamplerInfo.compareEnable = VK_FALSE;
    SamplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    SamplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    SamplerInfo.mipLodBias = 0.f;
    SamplerInfo.minLod = 0.f;
    SamplerInfo.maxLod = static_cast<float>(Tex.MipLevels);

    VK_CHECK_RESULT(vkCreateSampler(Device, &SamplerInfo, nullptr, &Tex.Sampler));

    return Tex;
}

Material VulkanContext::CreateMaterial(
    const std::vector<std::tuple<VkDescriptorType, VkShaderStageFlagBits>>& DescriptorBindings,
    const std::vector<std::tuple<const std::string&, VkShaderStageFlagBits>>& ShaderPaths,
    const VkPipelineVertexInputStateCreateInfo& VertexInfo,
    const VkPushConstantRange& PushConstRange, bool bShouldEnableDepth) const
{
    Material Mat;
    Mat.DescriptorBindings = DescriptorBindings;
    // Until I learn how to use multiple subpasses, all materials are subpass 0
    Mat.Subpass = 0;

    std::vector<VkDescriptorPoolSize> PoolSizes;
    for (auto [Type, Stage] : DescriptorBindings)
    {
        VkDescriptorPoolSize PoolSize{};
        PoolSize.type = Type;
        PoolSize.descriptorCount = 1;
        PoolSizes.push_back(PoolSize);
    }

    VkDescriptorPoolCreateInfo PoolCreateInfo{};
    PoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    PoolCreateInfo.poolSizeCount = static_cast<uint32_t>(PoolSizes.size());
    PoolCreateInfo.pPoolSizes = PoolSizes.data();
    // For now, allow 10 material instances to be created from this pool
    PoolCreateInfo.maxSets = 10 * static_cast<uint32_t>(PoolSizes.size());
    PoolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    VK_CHECK_RESULT(vkCreateDescriptorPool(Device, &PoolCreateInfo, nullptr, &Mat.DescriptorPool));

    int Binding = 0;
    std::vector<VkDescriptorSetLayoutBinding> SetLayoutBindings;
    for (auto [Type, Stage] : DescriptorBindings)
    {
        VkDescriptorSetLayoutBinding SetLayoutBinding{};
        SetLayoutBinding.descriptorType = Type;
        SetLayoutBinding.stageFlags = Stage;
        SetLayoutBinding.binding = Binding;
        SetLayoutBinding.descriptorCount = 1;
        Binding++;
        SetLayoutBindings.push_back(SetLayoutBinding);
    }
    VkDescriptorSetLayoutCreateInfo SetLayoutCreateInfo{};
    SetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    SetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(SetLayoutBindings.size());
    SetLayoutCreateInfo.pBindings = SetLayoutBindings.data();
    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(Device, &SetLayoutCreateInfo, nullptr, &Mat.DescriptorSetLayout));

    // Create the material's pipeline
    Mat.PushConstRange = PushConstRange;

    VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo{};
    PipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    PipelineLayoutCreateInfo.setLayoutCount = 1;
    PipelineLayoutCreateInfo.pSetLayouts = &Mat.DescriptorSetLayout;
    PipelineLayoutCreateInfo.pushConstantRangeCount = 1;
    PipelineLayoutCreateInfo.pPushConstantRanges = &PushConstRange;
    VK_CHECK_RESULT(vkCreatePipelineLayout(Device, &PipelineLayoutCreateInfo, nullptr, &Mat.PipelineLayout));

    VkPipelineInputAssemblyStateCreateInfo InputAssemblyState{};
    InputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    InputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    InputAssemblyState.primitiveRestartEnable = VK_FALSE;
    InputAssemblyState.flags = 0;

    VkPipelineRasterizationStateCreateInfo RasterizationState{};
    RasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    RasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    RasterizationState.cullMode = VK_CULL_MODE_NONE;
    RasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    RasterizationState.lineWidth = 1.f;
    RasterizationState.depthBiasEnable = VK_FALSE;
    RasterizationState.depthBiasConstantFactor = 0.f;
    RasterizationState.depthBiasClamp = 0.f;
    RasterizationState.depthBiasSlopeFactor = 0.f;
    RasterizationState.flags = 0;

    VkPipelineColorBlendAttachmentState BlendAttachmentState{};
    BlendAttachmentState.blendEnable = VK_TRUE;
    BlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    BlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    BlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    BlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
    BlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    BlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    BlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

    VkPipelineColorBlendStateCreateInfo ColorBlendState{};
    ColorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    ColorBlendState.attachmentCount = 1;
    ColorBlendState.pAttachments = &BlendAttachmentState;

    VkPipelineDepthStencilStateCreateInfo DepthStencilState{};
    DepthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    if (bShouldEnableDepth)
    {
        DepthStencilState.depthTestEnable = VK_TRUE;
        DepthStencilState.depthWriteEnable = VK_TRUE;
        DepthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
        DepthStencilState.depthBoundsTestEnable = VK_FALSE;
        DepthStencilState.minDepthBounds = 0.f;
        DepthStencilState.maxDepthBounds = 1.f;
        DepthStencilState.stencilTestEnable = VK_FALSE;
        DepthStencilState.front = {};
        DepthStencilState.back = {};
    }
    else
    {
        DepthStencilState.depthTestEnable = VK_FALSE;
        DepthStencilState.depthWriteEnable = VK_FALSE;
        DepthStencilState.depthCompareOp = VK_COMPARE_OP_ALWAYS;
        DepthStencilState.back.compareOp = VK_COMPARE_OP_ALWAYS;
    }
    
    VkPipelineViewportStateCreateInfo ViewportState{};
    ViewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    ViewportState.viewportCount = 1;
    ViewportState.scissorCount = 1;
    ViewportState.pViewports = &Viewport;
    ViewportState.pScissors = &Scissor;
    ViewportState.flags = 0;

    VkPipelineMultisampleStateCreateInfo MultisampleState{};
    MultisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    MultisampleState.rasterizationSamples = MSAASampleCount;
    MultisampleState.flags = 0;

    std::vector<VkDynamicState> DynamicStates = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR };
    
    VkPipelineDynamicStateCreateInfo DynamicState{};
    DynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    DynamicState.dynamicStateCount = static_cast<uint32_t>(DynamicStates.size());
    DynamicState.pDynamicStates = DynamicStates.data();
    DynamicState.flags = 0;

    for (auto & [ShaderPath, ShaderStage] : ShaderPaths)
    {
        Mat.Shaders.push_back(CreateShader(ShaderPath, ShaderStage));
    }

    VkGraphicsPipelineCreateInfo PipelineCreateInfo{};
    PipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    PipelineCreateInfo.layout = Mat.PipelineLayout;
    PipelineCreateInfo.renderPass = RenderPass;
    PipelineCreateInfo.pInputAssemblyState = &InputAssemblyState;
    PipelineCreateInfo.pRasterizationState = &RasterizationState;
    PipelineCreateInfo.pColorBlendState = &ColorBlendState;
    PipelineCreateInfo.pMultisampleState = &MultisampleState;
    PipelineCreateInfo.pViewportState = &ViewportState;
    PipelineCreateInfo.pDepthStencilState = &DepthStencilState;
    PipelineCreateInfo.pVertexInputState = &VertexInfo;
    PipelineCreateInfo.pDynamicState = &DynamicState;
    PipelineCreateInfo.stageCount = static_cast<uint32_t>(Mat.Shaders.size());
    PipelineCreateInfo.pStages = Mat.Shaders.data();
    PipelineCreateInfo.subpass = Mat.Subpass;

    VK_CHECK_RESULT(vkCreateGraphicsPipelines(Device, Mat.PipelineCache, 1, &PipelineCreateInfo, nullptr, &Mat.Pipeline));
    
    return Mat;
}

MaterialInstance VulkanContext::CreateMaterialInstance(const Material& Mat, const std::vector<std::vector<VkWriteDescriptorSet>>& WriteSets) const
{
    MaterialInstance MatInst;
    MatInst.RootMaterial = &Mat;
    MatInst.DescriptorSets.resize(SwapchainImages.size());
    MatInst.WriteSets = WriteSets;
    
    std::vector<VkDescriptorSetLayout> SetLayouts(SwapchainImages.size(), Mat.DescriptorSetLayout);

    VkDescriptorSetAllocateInfo AllocInfo{};
    AllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    AllocInfo.descriptorPool = Mat.DescriptorPool;
    AllocInfo.descriptorSetCount = static_cast<uint32_t>(SetLayouts.size());
    AllocInfo.pSetLayouts = SetLayouts.data();

    MatInst.DescriptorSets.resize(SwapchainImages.size());

    // Allocate descriptor sets for each frame index
    vkAllocateDescriptorSets(Device, &AllocInfo, MatInst.DescriptorSets.data());
    // For each swapchain image
    for (uint32_t SwapchainIndex = 0; SwapchainIndex < SwapchainImages.size(); ++SwapchainIndex)
    {        
        // Update all the descriptor sets in the material instance with corresponding write set
        uint32_t i = 0;
        for (VkWriteDescriptorSet& WriteSet : MatInst.WriteSets[SwapchainIndex])
        {
            WriteSet.dstSet = MatInst.DescriptorSets[SwapchainIndex];
            i++;
        }
        vkUpdateDescriptorSets(Device, static_cast<uint32_t>(MatInst.WriteSets[SwapchainIndex].size()), MatInst.WriteSets[SwapchainIndex].data(), 0, nullptr);
    }
    return MatInst;
}

void VulkanContext::DestroyMaterialInstance(MaterialInstance& MatInst) const
{
    vkFreeDescriptorSets(Device, MatInst.RootMaterial->DescriptorPool, static_cast<uint32_t>(MatInst.DescriptorSets.size()), MatInst.DescriptorSets.data());
}

void VulkanContext::DestroyMaterial(Material& Mat) const
{
    // Destroy shaders associated with the material
    for (VkPipelineShaderStageCreateInfo ShaderInfo : Mat.Shaders)
    {
        vkDestroyShaderModule(Device, ShaderInfo.module, nullptr);
    }
    
    vkDestroyPipeline(Device, Mat.Pipeline, nullptr);
    vkDestroyPipelineLayout(Device, Mat.PipelineLayout, nullptr);
    vkDestroyDescriptorSetLayout(Device, Mat.DescriptorSetLayout, nullptr);
    vkDestroyDescriptorPool(Device, Mat.DescriptorPool, nullptr);
}

void VulkanContext::DestroyTexture(Texture& Tex) const
{
    vkDestroyImageView(Device, Tex.View, nullptr);
    vkDestroyImage(Device, Tex.Image, nullptr);
    vkDestroySampler(Device, Tex.Sampler, nullptr);
    vkFreeMemory(Device, Tex.Memory, nullptr);
}


void VulkanContext::CreateImage(uint32_t width, uint32_t height, uint32_t mipLevels,
    VkSampleCountFlagBits numSamples,
    VkFormat format, VkImageTiling tiling,
    VkImageUsageFlags usage, 
    VkMemoryPropertyFlags properties, 
    VkImage& image, VkDeviceMemory& imageMemory) const
{
    VkImageCreateInfo imageInfo{};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = mipLevels;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = usage;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.samples = numSamples;

    VK_CHECK_RESULT(vkCreateImage(Device, &imageInfo, nullptr, &image));

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(Device, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = FindMemoryType(memRequirements.memoryTypeBits, properties);

    VK_CHECK_RESULT(vkAllocateMemory(Device, &allocInfo, nullptr, &imageMemory));

    vkBindImageMemory(Device, image, imageMemory, 0);
}

VkImageView VulkanContext::CreateImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) const
{
    VkImageViewCreateInfo viewInfo{};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = mipLevels;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView;
    VK_CHECK_RESULT(vkCreateImageView(Device, &viewInfo, nullptr, &imageView));
    return imageView;
}

void VulkanContext::CreateInitialCommandBuffers()
{
    ZoneScoped
    vkDeviceWaitIdle(Device);
    PrimaryCommandBuffers.resize(SwapchainFramebuffers.size());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = PrimaryCommandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = static_cast<uint32_t>(PrimaryCommandBuffers.size());

    VK_CHECK_RESULT(vkAllocateCommandBuffers(Device, &allocInfo, PrimaryCommandBuffers.data()));
}

void VulkanContext::CreateSyncPrimitives()
{
    ZoneScoped
    ImageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    RenderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    InFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    ImagesInFlight.resize(SwapchainImages.size(), VK_NULL_HANDLE);

    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        VK_CHECK_RESULT(vkCreateSemaphore(Device, &semaphoreInfo, nullptr, &ImageAvailableSemaphores[i]));
        VK_CHECK_RESULT(vkCreateSemaphore(Device, &semaphoreInfo, nullptr, &RenderFinishedSemaphores[i]));

        VK_CHECK_RESULT(vkCreateFence(Device, &fenceInfo, nullptr, &InFlightFences[i]));
    }
}

void VulkanContext::CreateTracyProfilerContext()
{
    VkCommandBufferAllocateInfo AllocInfo{};
    AllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    AllocInfo.commandPool = PrimaryCommandPool;
    AllocInfo.commandBufferCount = 1;

    VkCommandBuffer TracyCmdBuff;

    VK_CHECK_RESULT(vkAllocateCommandBuffers(Device, &AllocInfo, &TracyCmdBuff));

    TracyCtx = TracyVkContext(PhysicalDevice, Device, Queues.Graphics, TracyCmdBuff);

    vkFreeCommandBuffers(Device, PrimaryCommandPool, 1, &TracyCmdBuff);
}

void VulkanContext::InitVulkan()
{
    ZoneScoped
    CreateInstance();
    SetupDebugMessenger();
    CreateSurface();
    PickPhysicalDevice();
    CreateLogicalDevice();
    CreateSwapchain();
    CreateSwapchainViews();
    CreateRenderPass();
    CreateCommandPool();
    CreateColorResources();
    CreateDepthResources();
    CreateFramebuffers();
    CreateInitialCommandBuffers();
    CreateSyncPrimitives();

    CreateTracyProfilerContext();
}

void VulkanContext::CleanupSwapchain()
{
    ZoneScoped

    vkDestroyImageView(Device, ColorImageView, nullptr);
    vkDestroyImage(Device, ColorImage, nullptr);
    vkFreeMemory(Device, ColorImageMemory, nullptr);

    vkDestroyImageView(Device, DepthImageView, nullptr);
    vkDestroyImage(Device, DepthImage, nullptr);
    vkFreeMemory(Device, DepthImageMemory, nullptr);

    for (size_t i = 0; i < SwapchainFramebuffers.size(); ++i)
    {
        vkDestroyFramebuffer(Device, SwapchainFramebuffers[i], nullptr);
    }

    vkFreeCommandBuffers(Device, PrimaryCommandPool, static_cast<uint32_t>(PrimaryCommandBuffers.size()), PrimaryCommandBuffers.data());

    vkDestroyRenderPass(Device, RenderPass, nullptr);
    
    for (size_t i = 0; i < SwapchainImageViews.size(); ++i)
    {
        vkDestroyImageView(Device, SwapchainImageViews[i], nullptr);
    }

    vkDestroySwapchainKHR(Device, Swapchain, nullptr);
}

void VulkanContext::RecreateSwapchain()
{
    ZoneScoped
    // For now freeze the window when minimized
    int width = 0, height = 0;
    glfwGetFramebufferSize(WindowPtr->window, &width, &height);
    while (width == 0 || height == 0)
    {
        glfwGetFramebufferSize(WindowPtr->window, &width, &height);
        glfwWaitEvents();
    }

    vkDeviceWaitIdle(Device);

    CleanupSwapchain();

    CreateSwapchain();
    CreateSwapchainViews();
    CreateRenderPass();
    CreateColorResources();
    CreateDepthResources();
    CreateFramebuffers();
    CreateInitialCommandBuffers();
}

void VulkanContext::RerecordFrameCommandBuffers(uint32_t frameIndex)
{
    ZoneScoped

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;

    VK_CHECK_RESULT(vkBeginCommandBuffer(PrimaryCommandBuffers[frameIndex], &beginInfo));

    VkRenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = RenderPass;
    renderPassInfo.framebuffer = SwapchainFramebuffers[frameIndex];
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = SwapchainExtent;

    std::array<VkClearValue, 2> clearValues = {};
    clearValues[0].color = {0.1f, 0.1f, 0.1f, 1.0f};
    clearValues[1].depthStencil = {1.f, 0};
    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.data();

    {
        TracyVkZone(TracyCtx, PrimaryCommandBuffers[frameIndex], "Renderpass");
        vkCmdBeginRenderPass(PrimaryCommandBuffers[frameIndex], &renderPassInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
        
        for (const std::vector<VkCommandBuffer>* SecondaryCommandList : ExternalSecondaryBuffers)
        {
            vkCmdExecuteCommands(PrimaryCommandBuffers[frameIndex], 1, &SecondaryCommandList->at(frameIndex));
        }

        vkCmdExecuteCommands(PrimaryCommandBuffers[frameIndex], 1, &UICommandBuffers->at(frameIndex));

        vkCmdEndRenderPass(PrimaryCommandBuffers[frameIndex]);
    }

    TracyVkCollect(TracyCtx, PrimaryCommandBuffers[frameIndex]);

    VK_CHECK_RESULT(vkEndCommandBuffer(PrimaryCommandBuffers[frameIndex]));
}

void VulkanContext::PrepareNextFrame()
{
    ZoneScoped
    vkWaitForFences(Device, 1, &InFlightFences[CurrentFrameIndex], VK_TRUE, UINT64_MAX);

    VkResult result = vkAcquireNextImageKHR(Device, Swapchain, UINT64_MAX, ImageAvailableSemaphores[CurrentFrameIndex], VK_NULL_HANDLE, &ImageIndex);
    
    if (result == VK_ERROR_OUT_OF_DATE_KHR)
    {
        RecreateSwapchain();
        return;
    }
    else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    {
        std::cout << "FATAL: Failed to acquire swap chain image!" << std::endl;
        return;
    }
    return;
}

void VulkanContext::UpdateFrameData()
{
    ZoneScoped
    RerecordFrameCommandBuffers(ImageIndex);
}

void VulkanContext::PresentFrame()
{
    ZoneScoped
    // Put any per-frame tasks BEFORE this block
    {
        ZoneScopedN("WaitingNextImage")
        if (ImagesInFlight[ImageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(Device, 1, &ImagesInFlight[ImageIndex], VK_TRUE, UINT64_MAX);
        }
    }
    ImagesInFlight[ImageIndex] = InFlightFences[CurrentFrameIndex];

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore waitSemaphores[] = {ImageAvailableSemaphores[CurrentFrameIndex]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &PrimaryCommandBuffers[ImageIndex];

    VkSemaphore signalSemaphores[] = {RenderFinishedSemaphores[CurrentFrameIndex]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    {
        ZoneScopedN("SubmitRenderCommands")

        vkResetFences(Device, 1, &InFlightFences[CurrentFrameIndex]);

        VK_CHECK_RESULT(vkQueueSubmit(Queues.Graphics, 1, &submitInfo, InFlightFences[CurrentFrameIndex]));
    }

    {
        ZoneScopedN("PresentImage")
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;

        VkSwapchainKHR swapchains[] = {Swapchain};
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapchains;
        presentInfo.pImageIndices = &ImageIndex;
        presentInfo.pResults = nullptr;

        VkResult result = vkQueuePresentKHR(Queues.Present, &presentInfo);

        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
        {
            RecreateSwapchain();
        }
        else if (result != VK_SUCCESS)
        {
            std::cout << "FATAL: Failed to present swap chain image!" << std::endl;
            return;
        }
    }

    CurrentFrameIndex = (CurrentFrameIndex + 1) % MAX_FRAMES_IN_FLIGHT;
}

void VulkanContext::CleanupRenderingResources()
{
    ZoneScoped
    vkDeviceWaitIdle(Device);

    TracyVkDestroy(TracyCtx);

    CleanupSwapchain();

    
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        vkDestroySemaphore(Device, RenderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(Device, ImageAvailableSemaphores[i], nullptr);
        vkDestroyFence(Device, InFlightFences[i], nullptr);
    }

    vkDestroyCommandPool(Device, PrimaryCommandPool, nullptr);

    vkDestroyDevice(Device, nullptr);

    if (enableValidationLayers)
    {
        DestroyDebugUtilsMessengerEXT(Instance, DebugMessenger, nullptr);
    }

    vkDestroySurfaceKHR(Instance, Surface, nullptr);

    vkDestroyInstance(Instance, nullptr);
}