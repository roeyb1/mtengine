#ifndef VULKAN_CONTEXT_H
#define VULKAN_CONTEXT_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "ECS/Component.h"
#include "Components/Window.h"

#include "core/rid_manager.h"

#include <tracy/Tracy.hpp>
#include <tracy/TracyVulkan.hpp>

#include "VulkanTypes.h"

struct ModelPushConst
{
    glm::mat4 model = glm::mat4(1.f);
};

struct VertexArray
{
    Buffer vb;
    Buffer ib;
    
    uint32_t numIndices;
    glm::mat4 model;
};

class VulkanContext
{
    // This is atypical but I will do it for now until I come up with something better
    SINGLETON_COMPONENT(VulkanContext);
public:
    void InitVulkan();

    Texture CreateTexture(
        const std::string& Path, bool bShouldCreateMips,
        VkFormat Format = VK_FORMAT_R8G8B8A8_SRGB, VkImageTiling Tiling = VK_IMAGE_TILING_OPTIMAL) const;
    Texture CreateTexture(
        unsigned char* PixelData, uint32_t Width, uint32_t Height, bool bShouldCreateMips,
        VkFormat Format = VK_FORMAT_R8G8B8A8_SRGB, VkImageTiling Tiling = VK_IMAGE_TILING_OPTIMAL) const;
    
    void DestroyTexture(Texture& Tex) const;

    Material CreateMaterial(
        const std::vector<std::tuple<VkDescriptorType, VkShaderStageFlagBits>>& DescriptorBindings, 
        const std::vector<std::tuple<const std::string&, VkShaderStageFlagBits>>& ShaderPaths,
        const VkPipelineVertexInputStateCreateInfo& VertexInfo,
        const VkPushConstantRange& PushConstRange, bool bShouldEnableDepth) const;
    MaterialInstance CreateMaterialInstance(const Material& Mat, const std::vector<std::vector<VkWriteDescriptorSet>>& WriteSets) const;

    void DestroyMaterialInstance(MaterialInstance& MatInst) const;
    void DestroyMaterial(Material& Mat) const;

    void AllocateBuffer(VkDeviceSize Size, 
        VkBufferUsageFlags Usage, 
        VkMemoryPropertyFlags Properties, 
        VkBuffer& Buffer, 
        VkDeviceMemory& BufferMemory) const;
    
    Buffer CreateBuffer(VkDeviceSize Size,
        VkBufferUsageFlags Usage,
        VkMemoryPropertyFlags Properties) const;

    void DestroyBuffer(Buffer& Buffer) const;

    void CopyBufferToImage(VkBuffer Buffer, VkImage Image, uint32_t Width, uint32_t Height) const;
    void TransitionImageLayout(VkImage Image, VkFormat Format, VkImageLayout OldLayout, VkImageLayout NewLayout, uint32_t MipLevels) const;

    Buffer CreateVertexBuffer(const std::vector<Vertex>& Vertices) const;
    Buffer CreateIndexBuffer(const std::vector<uint32_t>& Indices) const;

    RID CreateDrawable(const std::vector<Vertex>& Vertices, const std::vector<uint32_t>& Indices, const glm::mat4& Model);

    VkPipelineShaderStageCreateInfo CreateShader(const std::string& Path, VkShaderStageFlagBits Stage) const;

    void RegisterSecondaryCommands(const std::vector<VkCommandBuffer>* SecondaryCommands) const;
    void RegisterUICommands(const std::vector<VkCommandBuffer>* UICommands) const;

    void PrepareNextFrame();
    void UpdateFrameData();
    void PresentFrame();

    void CleanupRenderingResources();


    /** Getters for common Vulkan resources **/
    const VkPhysicalDevice& GetPhysicalDevice() const { return PhysicalDevice; }
    const VkDevice& GetDevice() const { return Device; }
    const VkInstance& GetInstance() const { return Instance; }
    const VkSampleCountFlagBits GetSampleCount() const { return MSAASampleCount; }
    const VkRenderPass& GetRenderPass() const { return RenderPass; }
    const VkFramebuffer& GetActiveFramebuffer() const { return SwapchainFramebuffers[ImageIndex]; }
    const VkFramebuffer& GetFramebuffer(uint32_t Index) const { return SwapchainFramebuffers[Index]; }
    const uint32_t GetFrameIndex() const { return ImageIndex; }
    const VkViewport& GetViewport() const { return Viewport; }
    // Return an scissor which extends the entire viewport.
    const VkRect2D& GetScissor() const { return Scissor; }
    const VkQueue& GetGraphicsQueue() const { return Queues.Graphics; }
    const uint32_t GetGraphicsQueueFamily() const { return FindQueueFamilies(PhysicalDevice).GraphicsFamily.value(); }
    const uint32_t GetNumFrames() const { return SwapchainImages.size(); }
    void SetWindowPtr(Window* window) { WindowPtr = window; }
    VkCommandPool& GetCommandPool() { return PrimaryCommandPool; }
private:
    void CreateInstance();
    void SetupDebugMessenger();
    void CreateSurface();
    void PickPhysicalDevice();
    void CreateLogicalDevice();
    void CreateSwapchain();
    void CreateSwapchainViews();
    void CreateRenderPass();
    void CreateFramebuffers();
    void CreateCommandPool(); 
    void CreateColorResources();
    void CreateDepthResources();
    void CreateInitialCommandBuffers();
    void CreateSyncPrimitives();
    void CreateTracyProfilerContext();

    void CleanupSwapchain();
    void RecreateSwapchain();

    void RerecordFrameCommandBuffers(uint32_t FrameIndex);

    void CreateImage(
        uint32_t Width, uint32_t Height, uint32_t MipLevels,
        VkSampleCountFlagBits NumSamples,
        VkFormat Format, VkImageTiling Tiling, 
        VkImageUsageFlags Usage, 
        VkMemoryPropertyFlags Properties, 
        VkImage& Image, VkDeviceMemory& ImageMemory) const;

    VkImageView CreateImageView(
        VkImage Image, VkFormat Format, 
        VkImageAspectFlags AspectFlags, uint32_t MipLevels) const;

    void CopyBufferToBuffer(VkBuffer SrcBuff, VkBuffer DstBuff, VkDeviceSize Size) const;

    VkCommandBuffer BeginOneTimeCommands() const;
    void EndOneTimeCommands(VkCommandBuffer CommandBuffer) const;

    void GenerateMipmaps(VkImage Image, VkFormat Format, int32_t TexWidth, int32_t TexHeight, uint32_t MipLevels) const;

    struct QueueFamilyIndices
    {
        std::optional<uint32_t> GraphicsFamily;
        std::optional<uint32_t> PresentFamily;

        bool FoundAll()
        {
            return GraphicsFamily.has_value() && PresentFamily.has_value();
        }
    };
    QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice Device) const;
    bool CheckPhysicalDeviceSuitability(VkPhysicalDevice Device);

    VkFormat FindSupportedFormat(const std::vector<VkFormat>& Candidates, VkImageTiling Tiling, VkFormatFeatureFlags Features);

    VkInstance Instance;
    VkDebugUtilsMessengerEXT DebugMessenger;
    VkPhysicalDevice PhysicalDevice = VK_NULL_HANDLE;
    VkDevice Device;

    TracyVkCtx TracyCtx;

    struct
    {
        VkQueue Graphics;
        VkQueue Present;
    } Queues;

    struct SwapchainSupportDetails
    {
        VkSurfaceCapabilitiesKHR Capabilities;
        std::vector<VkSurfaceFormatKHR> Formats;
        std::vector<VkPresentModeKHR> PresentModes;
    };
    SwapchainSupportDetails QuerySwapchainSupport(VkPhysicalDevice PhysicalDevice);

    VkSurfaceFormatKHR ChooseSwapchainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& AvailableFormats);
    VkPresentModeKHR ChooseSwapchainPresentMode(const std::vector<VkPresentModeKHR>& AvailablePresentModes);
    VkExtent2D ChooseSwapchainExtent(const VkSurfaceCapabilitiesKHR& Capabilities);
    VkSurfaceKHR Surface;
    VkSwapchainKHR Swapchain;
    VkFormat SwapchainImageFormat;
    VkExtent2D SwapchainExtent;

    Window* WindowPtr;

    VkViewport Viewport{};
    VkRect2D Scissor{};

    std::vector<VkImage> SwapchainImages;
    std::vector<VkImageView> SwapchainImageViews;

    VkImage ColorImage;
    VkDeviceMemory ColorImageMemory;
    VkImageView ColorImageView;

    VkShaderModule CreateShaderModule(const std::vector<char>& ShaderCode) const;

    VkRenderPass RenderPass;
    VkSampleCountFlagBits MSAASampleCount = VK_SAMPLE_COUNT_1_BIT;
    VkSampleCountFlagBits GetMaxUsableSampleCount();

    std::vector<VkFramebuffer> SwapchainFramebuffers;

    VkCommandPool PrimaryCommandPool;
    std::vector<VkCommandBuffer> PrimaryCommandBuffers;

    // Use a lock to control the access to external secondary buffers so that systems can
    // retrieve VulkanContext as pointer-to-const
    mutable std::mutex SecondaryCmdMutex;
    // These buffers are managed by other parts of the codebase and so
    // we only store a pointer to the vector of CmdBuffers
    mutable std::vector<const std::vector<VkCommandBuffer>*> ExternalSecondaryBuffers;
    // These command buffers are specifically drawn last so that the UI can overlap regular draw objects
    mutable const std::vector<VkCommandBuffer>* UICommandBuffers;

    std::vector<VkSemaphore> ImageAvailableSemaphores;
    std::vector<VkSemaphore> RenderFinishedSemaphores;
    std::vector<VkFence> InFlightFences;
    std::vector<VkFence> ImagesInFlight;
    size_t CurrentFrameIndex = 0;
    uint32_t ImageIndex = 0;

    VkFormat FindCompatibleDepthFormat();
    bool HasStencilComponent(VkFormat Format);
    VkImage DepthImage;
    VkDeviceMemory DepthImageMemory;
    VkImageView DepthImageView;


    uint32_t FindMemoryType(uint32_t TypeFilter, VkMemoryPropertyFlags Properties) const;
};

#endif // VULKAN_CONTEXT_H