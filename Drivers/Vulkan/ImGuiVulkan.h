#ifndef IMGUI_VULKAN_H
#define IMGUI_VULKAN_H

#include "ECS/Component.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <imgui/imgui.h>
#include <imgui/examples/imgui_impl_vulkan.h>
#include <imgui/examples/imgui_impl_glfw.h>

#include "Drivers/Vulkan/VulkanContext.h"

struct ImGuiCtx
{
    SINGLETON_COMPONENT(ImGuiCtx);

    Material Mat;
    MaterialInstance MatInst;

    Texture Font;
    std::vector<Buffer> VertexBuffers;
    std::vector<size_t> VertexCounts;

    std::vector<Buffer> IndexBuffers;
    std::vector<size_t> IndexCounts;

    VkCommandPool CommandPool;
    std::vector<VkCommandBuffer> CommandBuffers;

    struct PushConstBlock 
    {
        glm::vec2 Scale = {0, 0};
        glm::vec2 Translate = {0, 0};
    };
    std::vector<PushConstBlock> PushConstBlocks;
};

#endif // IMGUI_VULKAN_H