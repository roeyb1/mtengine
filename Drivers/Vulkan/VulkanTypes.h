#ifndef VULKAN_TYPES_H
#define VULKAN_TYPES_H

#include <vulkan/vulkan.h>

#include <glm/glm.hpp>

struct Vertex
{
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec2 uv;

    static VkVertexInputBindingDescription GetBindingDescription()
    {
        VkVertexInputBindingDescription bindingDescription{};
        bindingDescription.binding = 0;
        bindingDescription.stride = sizeof(Vertex);
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return bindingDescription;
    }

    static std::array<VkVertexInputAttributeDescription, 3> GetAttributeDescriptions()
    {
        std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions;
        attributeDescriptions[0].binding = 0;
        attributeDescriptions[0].location = 0;
        attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[0].offset = offsetof(Vertex, pos);
        
        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof(Vertex, color);

        attributeDescriptions[2].binding = 0;
        attributeDescriptions[2].location = 2;
        attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
        attributeDescriptions[2].offset = offsetof(Vertex, uv);
        return attributeDescriptions;
    }
};

struct Texture
{
    VkImage Image = VK_NULL_HANDLE;
    VkDeviceMemory Memory = VK_NULL_HANDLE;
    VkImageView View = VK_NULL_HANDLE;
    VkSampler Sampler = VK_NULL_HANDLE;
    uint32_t MipLevels = 1;
};

struct Buffer
{
    VkBuffer Buffer = VK_NULL_HANDLE;
    VkDeviceMemory Memory = VK_NULL_HANDLE;
};

struct Material
{
    std::vector<std::tuple<VkDescriptorType, VkShaderStageFlagBits>> DescriptorBindings;
    std::vector<VkPipelineShaderStageCreateInfo> Shaders;
    VkPushConstantRange PushConstRange;

    VkDescriptorPool DescriptorPool = VK_NULL_HANDLE;
    VkDescriptorSetLayout DescriptorSetLayout;

    // TODO: Implement a set of per-material descriptor writes for things
    // which will not change between material instances

    VkPipelineLayout PipelineLayout = VK_NULL_HANDLE;
    VkPipeline Pipeline = VK_NULL_HANDLE;
    VkPipelineCache PipelineCache = VK_NULL_HANDLE;

    uint32_t Subpass = 0;

    void Bind(VkCommandBuffer& CommandBuffer) const
    {
        vkCmdBindPipeline(CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, Pipeline);
    }
};

struct MaterialInstance
{   
    const Material* RootMaterial;
    
    // Description of each descriptor write in the set
    std::vector<std::vector<VkWriteDescriptorSet>> WriteSets;

    // One copy of the descriptor set per swapchain image.
    // Required since some aspects of the write set may change between frames,
    // such as uniform buffers
    std::vector<VkDescriptorSet> DescriptorSets;

    void Bind(VkCommandBuffer& CommandBuffer, uint32_t FrameIndex, const void* PushConstBlock) const
    {
        vkCmdBindDescriptorSets(
            CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
            RootMaterial->PipelineLayout, 0,
            1, &DescriptorSets[FrameIndex], 0, nullptr);
        
        // TODO: Allow fragment shader push constants
        if (PushConstBlock)
        {
            vkCmdPushConstants(CommandBuffer, RootMaterial->PipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, RootMaterial->PushConstRange.size, PushConstBlock);
        }
    }
};

struct CameraUBO
{
    glm::mat4 Projection;
    glm::mat4 View;
};

#endif // VULKAN_TYPES_H