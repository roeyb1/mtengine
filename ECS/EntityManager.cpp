#include "EntityManager.h"

#include <tracy/Tracy.hpp>

EntityManager::EntityManager() :
    nextID(0),
    numEntities(0),
    map()
{
}

EID EntityManager::create()
{
    ZoneScoped
    if (numEntities < map.size())
    {
        // find the empty spot
        for (auto&[id, isActive] : map)
        {
            if (!isActive)
            {
                isActive = true;
                numEntities++;
                return id;
            }
        }
    }
    map[nextID] = true;
    numEntities++;
    return nextID++;
}

void EntityManager::remove(EID eid)
{
    ZoneScoped
    map[eid] = false;
    numEntities--;
}