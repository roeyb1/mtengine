#ifndef SYSTEM_SCHEDULER_H
#define SYSTEM_SCHEDULER_H

#include <queue>
#include <vector>
#include <unordered_set>
#include <unordered_map>

#include <atomic>
#include <condition_variable>
#include <mutex>

#include "core/os/memory.h"
#include "core/os/thread_safe_queue.h"
#include "Worker.h"

#include "ISystem.h"

class SystemScheduler
{
public:

    SystemScheduler();

    ~SystemScheduler();
    
    void register_system(ISystem* system);

    void schedule();

    void run_initialize();
    void run_update();
    void run_terminate();

    void print_graphviz() const;

    uint32_t GetNumberOfWorkers() const { return workers.size(); }
    uint32_t GetNumberOfSystems() const { return systems.size(); }
private:
    friend class ISystem;
    friend class Worker;

    void generate_dependency_pairs();

    void generate_graph();

    void compute_indegrees();

    void add_task(ISystem* readySys);

    /** Graph Management **/
    std::vector<ISystem*> systems;
    std::vector<ISystem*> independentSystems;
    // depencies in the form of (dependent, prerequisite);
    std::vector<std::tuple<ISystem*, ISystem*>> dependencies;
    std::unordered_map<ISystem*, std::unordered_set<ISystem*>> dependencyGraph;
    // Doesn't really need to be stored here, but do so anyways
    std::unordered_map<ISystem*, uint32_t> indegreeMap;

    /** Thread Management **/
    std::atomic<bool> running;
    std::vector<Worker*> workers;
    ThreadsafeQueue<ISystem*> workQueue;
    std::atomic<uint32_t> numberOfFinished = ATOMIC_FLAG_INIT;
    std::atomic<uint32_t> numberOfTasks = ATOMIC_FLAG_INIT;
    // Systems which have run in the current frame
    std::mutex MapMutex;
    std::unordered_map<ISystem*, bool> FrameSystemMap;
};

#endif // SYSTEM_SCHEDULER_H