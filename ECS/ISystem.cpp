#include "ISystem.h"
#include "SystemScheduler.h"

#include <tracy/Tracy.hpp>

void ISystem::notify()
{
    ZoneScoped
    dependentFinishedCount++;
    if (dependentFinishedCount == indegree)
    {
        scheduler->add_task(this);
        dependentFinishedCount = 0;
    }
}

void ISystem::signal_dependents()
{
    ZoneScoped
    for (auto& system : dependents)
    {
        system->notify();
    }
}

int32_t ISystem::compare(const ISystem* other) const
{
    ZoneScoped
    // 1. test for constness
    int32_t retval = 0;
    for (auto& [type, isConst] : componentSignature)
    {
        for (auto& [otherType, otherIsConst] : other->componentSignature)
        {
            // if type match was found, resolve the dependency
            if (type == otherType)
            {  
                // 1. attempt resolve by checking constness
                if (isConst == 1 && otherIsConst == 1) continue;
                else if (isConst == 1)
                {
                    return -1;
                }
                else if (otherIsConst == 1)
                {
                    return 1;
                }

                // 2. attempt resolve by checking size of dependency only if component type
                //    is not a fence type (ie. the map value is not -1).

                int8_t val = 0;
                auto it = componentSignature.find(type);
                if (it != componentSignature.end()) val = it->second;
                if (val == -1) continue; // must be a fence, so this isn't a conflict

                if (componentSignature.size() > other->componentSignature.size())
                    retval = 1;
                else
                {
                    retval = -1;
                }
            }
        }
    }

    return retval;
}