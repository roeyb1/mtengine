#include "SystemScheduler.h"

#include <tracy/Tracy.hpp>
#include <tracy/common/TracySystem.hpp>

SystemScheduler::SystemScheduler()
{
    running = true;
    int numberOfThreads = std::thread::hardware_concurrency() - 2;
    // clamp to a minimum of 1
    if (numberOfThreads < 1) numberOfThreads = 1;

    for (int i = 0; i < numberOfThreads; ++i)
    {
        Worker* worker = memnew(Worker(this));
        workers.push_back(worker);
    }
}

SystemScheduler::~SystemScheduler()
{
    running = false;
    workQueue.terminate();

    for (auto& worker : workers)
    {
        worker->join();
        memdelete(worker);
    }

    for (auto& system : systems)
    {
        memdelete(system);
    }

}

void SystemScheduler::register_system(ISystem* system)
{
    ZoneScoped
    systems.push_back(system);
}

void SystemScheduler::run_initialize()
{
    ZoneScoped
    for (auto& system : systems)
    {
        system->initialize();
    }
}

void SystemScheduler::run_update()
{
    ZoneScoped
    numberOfFinished = 0;
    for (auto& system : independentSystems)
    {
        workQueue.push(system);
    }

    while (numberOfFinished != numberOfTasks)
    {
        // non optimal, main thread should try to grab tasks as well
        std::this_thread::yield();
    }

    for (auto& system : systems)
    {
        system->on_frame_finish();
    }
    FrameSystemMap.clear();
}

void SystemScheduler::run_terminate()
{
    ZoneScoped
    // Termination should happen in reverse initialization order
    for (auto it = systems.rbegin(); it != systems.rend(); ++it) 
    {
        (*it)->terminate();
    }
}

void SystemScheduler::schedule()
{
    ZoneScoped
    generate_dependency_pairs();
    generate_graph();
    numberOfTasks = systems.size();
}

void SystemScheduler::print_graphviz() const
{
    ZoneScoped
    printf("digraph G {\n");
    for (const auto& system : systems)
    {
        if (system->dependents.empty())
        {
            printf("\"%s\"\n", system->name);
        }
        for (const auto& dependent: system->dependents)
        {
            printf("\"%s\" -> \"%s\"\n", system->name, dependent->name);
        }
    }
    printf("}\n");
}

void SystemScheduler::generate_dependency_pairs()
{
    ZoneScoped
    std::unordered_map<std::string, std::vector<ISystem*>> componentMap;
    for (auto& system : systems)
    {
        for (auto [name, constness] : system->componentSignature)
        {
            componentMap[name].push_back(system);
        }
    }

    // tuple(dependent, prerequisite);
    dependencies.clear();
    std::unordered_set<ISystem*> finished;
    for (auto i = systems.begin(); i != systems.end(); i++)
    {
        ISystem* systemA = (*i);
        for (auto j = (i + 1); j != systems.end(); j++)
        {   
            ISystem* systemB = (*j);
            int32_t result = systemA->compare(systemB);
            if (result < 0)
            {
                dependencies.push_back(std::make_pair(systemA, systemB));
            }
            else if (result > 0)
            {
                dependencies.push_back(std::make_pair(systemB, systemA));
            }
            
        }
    }

    // cull redundant dependencies
    // for each vertex v:
    //     for each vertex u reachable by depth-first-search:
    //         remove edge(u, v)
}

void SystemScheduler::generate_graph()
{
    ZoneScoped
    // build the graph
    for (auto& [dependent, prereq] : dependencies)
    {
        dependencyGraph[prereq].insert(dependent);
    }

    compute_indegrees();
}

void SystemScheduler::compute_indegrees()
{
    ZoneScoped
    for (auto [system, dependents] : dependencyGraph)
    {
        for (auto dependent : dependents)
        {
            indegreeMap[dependent]++;

            system->dependents.push_back(dependent);
        }
    }

    // may not be required
    for (auto [system, indegree] : indegreeMap)
    {
        system->indegree = indegree;
    }

    for (auto& system : systems)
    {
        if (system->indegree == 0)
            independentSystems.push_back(system);
    }
}

void SystemScheduler::add_task(ISystem* readySys)
{
    ZoneScoped
    std::lock_guard<std::mutex> Lock(MapMutex);
    if (FrameSystemMap.contains(readySys))
    {
        return;
    }
    workQueue.push(readySys);
    FrameSystemMap[readySys] = true;
}