#ifndef ENTITY_H
#define ENTITY_H

#include "EntityManager.h"

#include <tracy/Tracy.hpp>

class Stage;

template <typename ...Ts>
class EntityProxy
{
public:
    static_assert(sizeof...(Ts), "Component list cannot be empty!");

    EntityProxy(const Stage* stage, EID eid) : eid(eid), stage(stage) {}

    template <typename T>
    static constexpr bool is_const_for() 
    {
        return (std::is_same_v<Ts, std::add_const_t<T>> || ...);
    }

    template <typename T>
    inline T* get_mutable() const
    {
        ZoneScoped
        static_assert(std::disjunction_v<std::is_same<T, std::remove_const_t<Ts>>...>, 
            "Cannot retrieve proxy for component which is not in proxy signature!");
            
        if constexpr(is_const_for<T>())
            static_assert(std::is_const_v<T>, 
                "Attempting to retrieve a non-const pointer to a component which should be const!");
        return T::get(eid);
    }

    template <typename T>
    inline const T* get_const() const
    {
        ZoneScoped
        static_assert(std::disjunction_v<std::is_same<T, std::remove_const_t<Ts>>...>, 
            "Cannot retrieve proxy for component which is not in proxy signature!");
        return T::get(eid);
    }

    template <typename T>
    inline auto get() const
    {
        ZoneScoped
        static_assert(T::isComponent);

        if constexpr(is_const_for<T>())
        {
            return get_const<T>();
        }
        else
        {
            return get_mutable<T>();
        }
    }

    const EID eid;
private:
    const Stage* stage;
};


struct IEntity
{
    IEntity(EID eid) : eid(eid) {}
    virtual ~IEntity() {}

    const EID eid;
};

template <typename ...Ts>
struct Entity : public IEntity
{
    Entity(EID eid) : IEntity(eid) {}
    
    ~Entity()
    {
        (Ts::remove(eid), ...);
        EntityManager::get().remove(eid);
    }
};

#endif // ENTITY_H