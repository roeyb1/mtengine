#ifndef SYSTEM_H
#define SYSTEM_H

#include "Stage.h"
#include "ISystem.h"
#include "Event.h"

#include <unordered_map>

#include <tracy/Tracy.hpp>

class SystemScheduler;

template <typename ...Ts>
class System : public ISystem
{
public:
    System()
    {
        if (name == nullptr)
            name = typeid(this).name();

        (([this]<class T>()
        {
            if constexpr(T::isComponent || std::is_const_v<T>)
            {
                componentSignature.insert(std::make_pair(typeid(T).name(), std::is_const_v<T>));
            }
            else
            {
                // component must be of fence type, therefore map val should be -1
                componentSignature.insert(std::make_pair(typeid(T).name(), -1));
            }
        }).template operator()<Ts>(), ...);
    }

    template <typename T>
    static constexpr bool is_const_for() 
    { 
        return (std::is_same_v<Ts, std::add_const_t<T>> || ...);
    }

    std::vector<EntityProxy<Ts...>> get_proxies() const
    {
        return stage->get_proxies<Ts...>();
    }

    template <typename T>
    std::vector<EntityProxy<T>> get_partial_proxy() const
    {
        ZoneScoped
        static_assert(std::disjunction_v<std::is_same<std::remove_const_t<T>, std::remove_const_t<Ts>>...>,
            "Trying to get partial proxies for component type which is not part of this system's signature!");
        
        return stage->get_proxies<T>();
    }

    template <typename T>
    T* get_singleton() const
    {
        ZoneScoped
        static_assert(std::disjunction_v<std::is_same<std::remove_const_t<T>, std::remove_const_t<Ts>>...>,
            "Trying to get singleton which is not part of this system's signature!");
        static_assert(T::isSingleton);
        static_assert(!is_const_for<T>());
        return T::get_mutable();
    }

    template<typename T>
    const T* get_singleton_const() const
    {
        ZoneScoped
        static_assert(T::isSingleton);
        return T::get_const();
    }

    std::tuple<EID, std::add_pointer_t<Ts>...> create_entity()
    {
        ZoneScoped
        return stage->create<Ts...>();
    }

    template <typename ...Us>
    std::tuple<EID, std::add_pointer_t<Us>...> create_entity()
    {
        ZoneScoped
        return stage->create<Us...>();
    }

    template <typename T>
    T* create_event()
    {
        ZoneScoped
        static_assert(T::isEvent);
        return T::create();
    }

    template <typename T>
    bool has_event() const
    {
        ZoneScoped
        static_assert(T::isEvent);
        return T::has();
    }

    template <typename T>
    const std::vector<T>& get_events() const
    {
        ZoneScoped
        static_assert(T::isEvent);
        return T::events();
    }

    // retrieve any newly created entities with this signature
    // WARNING: this will *NOT* enforce const-ness
    template <typename ...Us>
    std::vector<EntityProxy<Us...>> get_new_proxies() const
    {
        ZoneScoped
        //static_assert(std::disjunction_v<std::is_same<std::remove_const_t<Us>, std::remove_const_t<Ts>>...>);
        return stage->get_new_proxies<Us...>();
    }

    virtual void on_frame_finish() override final
    {
        ZoneScoped
        // May need to cleanup on registered types (specifically useful for event clearing)
        (([this]<class T>()
        {
            if constexpr(T::isEvent)
            {
                T::cleanup();
            }
        }).template operator()<Ts>(), ...);
    }

    void signal_quit()
    {
        ZoneScoped
        stage->signal_quit();
    }

protected:
    friend class Stage;
    Stage* stage;
};

#endif // SYSTEM_H