#ifndef WORKER_H
#define WORKER_H

#include <thread>

class SystemScheduler;

class Worker
{
public:
    Worker(SystemScheduler* scheduler);

    void worker_main();

    void join();

private:
    friend class SystemScheduler;
    SystemScheduler* scheduler;

    std::thread thread;
};

#endif // WORKER_H