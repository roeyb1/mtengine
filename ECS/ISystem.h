#ifndef ISYSTEM_H
#define ISYSTEM_H

#include <vector>
#include <atomic>
#include <unordered_map>

class SystemScheduler;

class ISystem
{
public:
    virtual void initialize() {}
    virtual void update() {}
    virtual void terminate() {}

    virtual void on_frame_finish() {}

    void notify();

    void signal_dependents();

    // return -1 if this depends on other, 1 if other depends on this,
    // 0 if there is no dependency
    int32_t compare(const ISystem* other) const;

    const char* get_name() const { return name; }
protected:
    friend class SystemScheduler;
    friend class Stage;
    const char* name = nullptr;
    SystemScheduler* scheduler;
    std::vector<ISystem*> dependents;
    std::atomic<uint32_t> indegree = ATOMIC_FLAG_INIT;
    std::atomic<uint32_t> dependentFinishedCount = ATOMIC_FLAG_INIT;

    // values in this map are 0 for nonconst, 1 for const, and -1 for fence types
    std::unordered_map<std::string, int8_t> componentSignature;
};

#endif // ISYSTEM_H