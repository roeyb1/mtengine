#ifndef STAGE_H
#define STAGE_H

#include <vector>
#include <tuple>
#include "SystemScheduler.h"
#include "EntityManager.h"
#include "Entity.h"

#include "core/os/memory.h"
#include <tracy/Tracy.hpp>

class ISystem;

class Stage
{
public:
    ~Stage();

    template <typename ...Ts>
    std::tuple<EID, std::add_pointer_t<Ts>...> create()
    {
        ZoneScoped
        EID newEntity = EntityManager::get().create();
        Entity<Ts...>* e = memnew(Entity<Ts...>(newEntity));
        responsibleForEntities.push_back(e);
        newEntities.push_back(e);
        return std::make_tuple(newEntity, Ts::create(newEntity)...);
    }

    template <typename ...Ts>
    std::vector<EntityProxy<Ts...>> get_proxies() const
    {
        ZoneScoped
        static_assert(sizeof...(Ts), "Component list cannot be empty!");
        std::vector<EntityProxy<Ts...>> result;
        // get all the entities which fit this archetype
        for (auto& entity : responsibleForEntities)
        {
            EID eid = entity->eid;
            if ((Ts::has(eid) && ...))
            {
                result.push_back(EntityProxy<Ts...>(this, eid));
            }
        }
        return result;
    }

    template <typename ...Ts>
    std::vector<EntityProxy<Ts...>> get_new_proxies() const
    {
        ZoneScoped
        static_assert(sizeof...(Ts), "Component list cannot be empty!");
        std::vector<EntityProxy<Ts...>> result;
        // get all the new entities which fit this archetype
        for (auto& entity : newEntities)
        {
            EID eid = entity->eid;
            if ((Ts::has(eid) && ...))
            {
                result.push_back(EntityProxy<Ts...>(this, eid));
            }
        }
        return result;
    }

    template <typename T>
    void register_system()
    {
        ZoneScoped
        static_assert(std::is_base_of_v<ISystem, T>);
        T* sys = memnew(T);
        sys->stage = this;
        sys->scheduler = &scheduler;
        sys->name = typeid(T).name();
        scheduler.register_system(sys);
    }

    template <typename T>
    T* create_event()
    {
        ZoneScoped
        static_assert(T::isEvent);
        return T::create();
    }

    void initialize();

    bool update();

    void terminate();

    void signal_quit() { isRunning = false; }

    inline const SystemScheduler& get_scheduler() const { return scheduler; }
    uint32_t GetNumberOfWorkers() const { return scheduler.GetNumberOfWorkers(); }
    uint32_t GetNumberOfSystems() const { return scheduler.GetNumberOfSystems(); }
private:
    SystemScheduler scheduler;
    std::vector<IEntity*> responsibleForEntities;
    std::vector<IEntity*> newEntities;

    std::atomic<bool> isRunning = ATOMIC_FLAG_INIT;
};

#endif // STAGE_H