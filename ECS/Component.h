#ifndef COMPONENT_H
#define COMPONENT_H

#include <unordered_map>
#include <tracy/Tracy.hpp>
#include <vector>

#include "Entity.h"

#define COMPONENT_DEFINITION(type)              \
private:                                        \
    friend class Stage;                             \
    template <typename ...Ts>                       \
    friend class EntityProxy;                       \
    template <typename ...Ts>                       \
    friend class Entity;                            \
    template <typename ...Ts>                       \
    friend class System;                            \
    static inline std::unordered_map<EID, uint64_t>& map() { static std::unordered_map<EID, uint64_t> m; return m; }\
    static inline std::vector<type>& data() { static std::vector<type> d(1000); return d; }\
    static type* get(EID entity)                \
    {                                           \
        return &data()[map()[entity]];          \
    }                                           \
    static bool has(EID entity)                 \
    {                                           \
        return (map().find(entity) != map().end());\
    }                                           \
    static type* create(EID entity)             \
    {                                           \
        data().push_back(type());               \
        map()[entity] = data().size() - 1;      \
        return &data().back();                  \
    }                                           \
    static EID owner(const type* component)     \
    {                                           \
        for (const auto& pair : map())          \
        {                                       \
            if (&data()[pair.second] == component) return pair.first;\
        }                                       \
        return INVALID_EID;                     \
    }                                           \
    static void remove(EID entity)              \
    {                                           \
        size_t removedIndex = map()[entity];    \
                                                \
        type* back = &data().back();            \
        EID backOwner = owner(back);            \
                                                \
        data()[removedIndex] = *back;           \
        map()[backOwner] = removedIndex;        \
                                                \
        data().pop_back();                      \
                                                \
        map().erase(map().find(entity));        \
                                                \
    }                                           \
    constexpr static bool isComponent = true;   \
    constexpr static bool isSingleton = false;  \
    constexpr static bool isEvent = false;      \
public:

#define FENCE_DEFINITION(type)                      \
struct type {                                       \
private:                                            \
    friend class Stage;                             \
    template <typename ...Ts> friend class EntityProxy;\
    template <typename ...Ts> friend class System;  \
    static bool has(EID) { return true; }           \
    constexpr static bool isComponent = false;      \
    constexpr static bool isSingleton = false;      \
    constexpr static bool isEvent = false;          \
};

#define SINGLETON_COMPONENT(type)                   \
private:                                            \
    friend class Stage;                             \
    template <typename ...Ts> friend class System;  \
    static type* get_mutable() { static type instance; return &instance; }  \
    static const type* get_const() { return get_mutable(); }                \
    static bool has(EID) { return true; }           \
    constexpr static bool isSingleton = true;       \
    constexpr static bool isComponent = true;       \
    constexpr static bool isEvent = false;          \
public:

#endif // COMPONENT_H