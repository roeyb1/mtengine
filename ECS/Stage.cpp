#include "Stage.h"

#include <tracy/Tracy.hpp>

Stage::~Stage()
{
    for (auto& entity : responsibleForEntities)
    {
        memdelete(entity);
    }
}

void Stage::initialize()
{
    ZoneScoped
    scheduler.schedule();
    scheduler.run_initialize();

    isRunning = true;
}

bool Stage::update()
{  
    ZoneScoped
    scheduler.run_update();
    newEntities.clear();

    FrameMark;
    return isRunning;
}

void Stage::terminate()
{
    ZoneScoped
    scheduler.run_terminate();
}