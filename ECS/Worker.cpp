#include "Worker.h"

#include "SystemScheduler.h"

#include <tracy/Tracy.hpp>

Worker::Worker(SystemScheduler* scheduler) :
    scheduler(scheduler),
    thread(&Worker::worker_main, this)
{
}

void Worker::worker_main()
{
    tracy::SetThreadName("Worker");
    ISystem* nextSys = nullptr;
    while (true)
    {
        scheduler->workQueue.wait_and_pop(nextSys);
        if (!scheduler->running) break;

        nextSys->update();
        nextSys->signal_dependents();
        scheduler->numberOfFinished++;
    }
}

void Worker::join()
{
    thread.join();
}