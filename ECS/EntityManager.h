#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include <cstdint>
#include <map>

using EID = uint64_t;

#define INVALID_EID UINT64_MAX

class EntityManager
{
public:
    EntityManager();
    
    EID create();

    void remove(EID eid);

    inline uint64_t get_num_entities() { return numEntities; }

    static EntityManager& get() { static EntityManager instance; return instance; }
private:
    std::map<EID, bool> map;
    EID nextID;
    uint64_t numEntities;
};

#endif // ENTITY_MANAGER_H