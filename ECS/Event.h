#ifndef EVENT_H
#define EVENT_H

#include <vector>

#define EVENT_DEFINITION(type)  \
template <typename ...Ts>       \
friend class System;            \
static inline std::vector<type>& events() { static std::vector<type> events; return events; }\
static bool has() { return !events().empty(); }\
static void cleanup()           \
{                               \
    events().clear();           \
}                               \
static type* create()           \
{                               \
    events().push_back(type()); \
    return &events().back();    \
}                               \
constexpr static bool isComponent = true;   \
constexpr static bool isEvent = true;       \
public:

#endif // EVENT_H
